#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script containing tests for the energyparsingvalues script.

@author: mark
"""

import unittest
import logging
import pathlib
from lxml import etree
import phasexmlparser.parsers.energyvaluesparsing as energyvaluesparsing

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class EnergyParsingValuesTestCase(unittest.TestCase):
    """Test case for energyparsingvalues
    """

    def setUp(self):
        """Test set up for the parsing of energy values.
        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.free_energy_file = (
            (parent_directory / "resources/energyvaluestest.xml").absolute().as_posix()
        )
        self.binding_energy_file = (
            (parent_directory / "resources/energyvaluestest2.xml").absolute().as_posix()
        )
        self.example_freeenergy_xml = """<phase:FreeEnergy xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" phase:moleculeID="-14.100solute" phase:fromSolventID="" phase:toSolventID="water" phase:valueType="MOLEFRACTION">
        <phase:TotalEnergy>-34.8195308084233</phase:TotalEnergy>
            <phase:EnergyContributions>
                <phase:EnergyContribution phase:ssipID="1">-34.8195308084233</phase:EnergyContribution>
            </phase:EnergyContributions>
        </phase:FreeEnergy>
  """
        self.example_bindingenergy_xml = """<phase:BindingEnergy xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" phase:moleculeID="-14.100solute" phase:fromSolventID="" phase:toSolventID="water" phase:valueType="MOLEFRACTION">
            <phase:TotalEnergy>-34.8195308084233</phase:TotalEnergy>
            <phase:EnergyContributions>
                <phase:EnergyContribution phase:ssipID="1">-34.8195308084233</phase:EnergyContribution>
            </phase:EnergyContributions>
        </phase:BindingEnergy>
  """
        self.example_freeenergy_element = etree.XML(self.example_freeenergy_xml)
        self.example_bindingenergy_element = etree.XML(self.example_bindingenergy_xml)

    def tearDown(self):
        """Tear down after test.
        """
        del self.example_freeenergy_element
        del self.example_freeenergy_xml
        del self.example_bindingenergy_element
        del self.example_bindingenergy_xml

    def test_parse_free_energy_elements_info_from_file(self):
        """Test to see if expected list of free energies is read in from the file.
        """
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
        }
        actual_dict_dict = energyvaluesparsing.parse_free_energy_elements_info_from_file(
            self.free_energy_file
        )
        self.assertListEqual(["-14.100solute//water"], sorted(actual_dict_dict.keys()))
        actual_dict = actual_dict_dict["-14.100solute//water"]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])
        solv_array = energyvaluesparsing.parse_energy_elements_from_file(
            self.free_energy_file,
            energyvaluesparsing.create_xpath_expression(
                energyvaluesparsing.FREE_ENERGY_XPATH, from_solv_id="a"
            ),
        )
        self.assertEqual(0, len(solv_array))

    def test_parse_binding_energy_elements_info_from_file(self):
        """Test to see if expected list of energies is read in from the file.
        """
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
        }
        actual_dict_dict = energyvaluesparsing.parse_binding_energy_elements_info_from_file(
            self.binding_energy_file
        )
        self.assertListEqual(["-14.100solute//water"], sorted(actual_dict_dict.keys()))
        actual_dict = actual_dict_dict["-14.100solute//water"]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])

    def test_create_xpath_expression(self):
        """Test to see if expected expression is returned
        """
        self.assertEqual(
            energyvaluesparsing.FREE_ENERGY_XPATH,
            energyvaluesparsing.create_xpath_expression(
                energyvaluesparsing.FREE_ENERGY_XPATH
            ),
        )
        self.assertEqual(
            "/phase:EnergyValues/phase:FreeEnergyCollection/phase:FreeEnergy[@phase:fromSolventID='']",
            energyvaluesparsing.create_xpath_expression(
                energyvaluesparsing.FREE_ENERGY_XPATH, from_solv_id=""
            ),
        )

    def test_parse_energy_element_list(self):
        """Test to see if expected list of energies is extracted.
        """
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
        }
        actual_dict_dict = energyvaluesparsing.parse_energy_element_list(
            [self.example_bindingenergy_element]
        )
        self.assertListEqual(["-14.100solute//water"], sorted(actual_dict_dict.keys()))
        actual_dict = actual_dict_dict["-14.100solute//water"]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])

    def test_parse_energy_element(self):
        """Test to see if expected dict is produced.
        """
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
        }
        actual_dict = energyvaluesparsing.parse_energy_element(
            self.example_freeenergy_element
        )
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])

    def test_get_totalenergy(self):
        """Test to see if expected total energy is read out.
        """
        expected_total_energy = -34.8195308084233
        actual_total_energy = energyvaluesparsing.get_totalenergy(
            self.example_freeenergy_element
        )
        self.assertAlmostEqual(expected_total_energy, actual_total_energy)

    def test_get_valuetype(self):
        """Test to see if expected vlauetype is returned.
        """
        expected_valuetype = "MOLEFRACTION"
        actual_valuetype = energyvaluesparsing.get_valuetype(
            self.example_freeenergy_element
        )
        self.assertEqual(expected_valuetype, actual_valuetype)

    def test_get_to_solventid(self):
        """Test to see if expected value is returned.
        """
        expected_id = "water"
        actual_id = energyvaluesparsing.get_to_solventid(
            self.example_freeenergy_element
        )
        self.assertEqual(expected_id, actual_id)

    def test_get_from_solventid(self):
        """Test to see if expected value is returned.
        """
        expected_id = ""
        actual_id = energyvaluesparsing.get_from_solventid(
            self.example_freeenergy_element
        )
        self.assertEqual(expected_id, actual_id)

    def test_get_molecule_id(self):
        """Test to see if expected StdInChIKey is returned.
        """
        expected_molecule_id = "-14.100solute"
        actual_molecule_id = energyvaluesparsing.get_molecule_id(
            self.example_freeenergy_element
        )
        self.assertEqual(expected_molecule_id, actual_molecule_id)

    def test_parse_energy_elements_from_file(self):
        """Test to see if expected free energy elements are found.
        """
        actual_xml_list = energyvaluesparsing.parse_energy_elements_from_file(
            self.free_energy_file, energyvaluesparsing.FREE_ENERGY_XPATH
        )
        self.assertEqual(1, len(actual_xml_list))
        for actual_xml in actual_xml_list:
            self.assertEqual(self.example_freeenergy_xml, etree.tounicode(actual_xml))

    def test_get_energy_elements(self):
        """Test to see if expected FreeEnergy Elements are found.
        """
        element_tree = energyvaluesparsing.xmlvalidation.validate_and_read_xml_file(
            self.free_energy_file, energyvaluesparsing.xmlvalidation.PHASE_SCHEMA
        )
        actual_xml_list = energyvaluesparsing.get_energy_elements(
            element_tree, energyvaluesparsing.FREE_ENERGY_XPATH
        )
        self.assertEqual(1, len(actual_xml_list))
        for actual_xml in actual_xml_list:
            self.assertEqual(self.example_freeenergy_xml, etree.tounicode(actual_xml))
