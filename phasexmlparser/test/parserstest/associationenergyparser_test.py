#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script

@author: mark
"""

import unittest
import logging
from lxml import etree
import numpy as np
import pathlib
import xmlvalidator.xmlvalidation as xmlval
import phasexmlparser.parsers.associationenergyparser as assocparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


class AssociationEnergyParserTestCase(unittest.TestCase):
    """Test case for association energy parsing.
    """

    def setUp(self):
        """Set up before tests
        """
        self.maxDiff = None
        self.association_filename = (
            (pathlib.Path(__file__).parents[1] / "resources/associationvalues.xml")
            .absolute()
            .as_posix()
        )
        self.xmlfilecontents = xmlval.validate_and_read_xml_file(
            self.association_filename, xmlval.PHASE_SCHEMA
        )
        self.xpath = assocparser.generate_association_element_xpath()
        self.element_list = assocparser.find_association_energy_elements(
            self.xmlfilecontents, self.xpath
        )
        self.example_element = self.element_list[1]

    def tearDown(self):
        """Clean up after tests.
        """
        del self.association_filename
        del self.xmlfilecontents
        del self.xpath
        del self.element_list
        del self.example_element

    def test_generate_data_matrices(self):
        """Test to see if expected arrays are created.
        """
        expected_array_dict = {
            "x_data": np.array([[1.30, 1.300]]),
            "y_data": np.array([[1.3, 4.9]]),
            "z_data": np.array([[10.222313287855103, 14.902313287855105]]),
        }
        actual_list = assocparser.parse_elements_from_file(
            self.association_filename, "water"
        )
        LOGGER.debug(actual_list)
        actual_dict = assocparser.restructure_data(actual_list)
        actual_array_dict = assocparser.generate_data_matrices(actual_dict)
        for key, value in expected_array_dict.items():
            with self.subTest(key=key):
                np.testing.assert_array_almost_equal(value, actual_array_dict[key])

    def test_restructure_data(self):
        """Test to see if restructure is completed as expected.
        """
        expected_dict = {
            "1.300": {
                "1.300": {
                    "solvent_id": "water",
                    "solute1": "1.300solute1",
                    "solute2": "1.300solute2",
                    "value": 10.222313287855103,
                    "temp": "298.000KELVIN",
                },
                "4.900": {
                    "solvent_id": "water",
                    "solute1": "1.300solute1",
                    "solute2": "4.900solute2",
                    "value": 14.902313287855105,
                    "temp": "298.000KELVIN",
                },
            }
        }
        assocparser.LOGGER.setLevel(logging.DEBUG)
        actual_list = assocparser.parse_elements_from_file(
            self.association_filename, "water"
        )
        LOGGER.debug(actual_list)
        actual_dict = assocparser.restructure_data(actual_list)
        self.assertDictEqual(expected_dict, actual_dict)

    def test_parse_elements_from_file(self):
        """Test to see if expected elements are returned.
        """
        expected_list = [
            {
                "solvent_id": "water",
                "solute1": "1.300solute1",
                "solute2": "1.300solute2",
                "value": 10.222313287855103,
                "temp": "298.000KELVIN",
            },
            {
                "solvent_id": "water",
                "solute1": "1.300solute1",
                "solute2": "4.900solute2",
                "value": 14.902313287855105,
                "temp": "298.000KELVIN",
            },
        ]

        actual_list = assocparser.parse_elements_from_file(
            self.association_filename, "water"
        )
        self.assertEqual(2, len(actual_list))
        for i in range(len(actual_list)):
            with self.subTest(i=i):
                self.assertDictEqual(expected_list[i], actual_list[i])

    def test_sift_association_information(self):
        """Test to see if elements are sifted as expected, so only those of interest are kept.
        """
        expected_list = [
            {
                "solvent_id": "water",
                "solute1": "1.300solute1",
                "solute2": "1.300solute2",
                "value": 10.222313287855103,
                "temp": "298.000KELVIN",
            },
            {
                "solvent_id": "water",
                "solute1": "1.300solute1",
                "solute2": "4.900solute2",
                "value": 14.902313287855105,
                "temp": "298.000KELVIN",
            },
        ]
        full_list = assocparser.parse_association_elements(self.element_list)
        actual_list = assocparser.sift_association_information(full_list, "water")
        for i in range(len(actual_list)):
            with self.subTest(i=i):
                self.assertDictEqual(expected_list[i], actual_list[i])

    def test_parse_association_elements(self):
        """Test to see if expected elements are returned.
        """
        actual_list = assocparser.parse_association_elements(self.element_list)
        self.assertEqual(18, len(actual_list))

    def test_parse_energy_information(self):
        """Test to see if expected information is parsed.
        """
        expected_dict = {
            "solvent_id": "water",
            "solute1": "1.300solute2",
            "solute2": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "value": 29.709250675914273,
            "temp": "298.000KELVIN",
        }
        actual_dict = assocparser.parse_energy_information(self.example_element)
        self.assertDictEqual(expected_dict, actual_dict)

    def test_parse_solvent_id(self):
        """Test to see if solvent ID is parsed as expected.
        """
        expected_values = ("water", "298.000KELVIN")
        actual_values = assocparser.parse_solvent_id(
            "water_1.300solute1_1.300solute2298.000KELVIN"
        )
        self.assertEqual(expected_values, actual_values)

    def test_get_solvent_id(self):
        """Test to see if solvent ID is returned as expected.
        """
        expected_value = "water_1.300solute1_1.300solute2298.000KELVIN"
        actual_value = assocparser.get_solvent_id(self.example_element)
        self.assertEqual(expected_value, actual_value)

    def test_get_energy_value(self):
        """Test to see if expected energy value is returned.
        """
        expected_value = 29.709250675914273
        actual_value = assocparser.get_energy_value(self.example_element)
        self.assertAlmostEqual(expected_value, actual_value)

    def test_get_solute2(self):
        """Test to see if expected solute ID 2 is found
        """
        expected_value = "XLYOFNOQVPJJNP-UHFFFAOYSA-N"
        actual_value = assocparser.get_solute2(self.example_element)
        self.assertEqual(expected_value, actual_value)

    def test_get_solute1(self):
        """Test to see if expected solute ID 1 is found.
        """
        expected_value = "1.300solute2"
        actual_value = assocparser.get_solute1(self.example_element)
        self.assertEqual(expected_value, actual_value)

    def test_generate_association_element_xpath(self):
        """Test to see if expected xpath is created.
        """
        expected_xpath = (
            "/phase:AssociationEnergyValueCollection/phase:AssociationEnergyValue"
        )
        self.assertEqual(expected_xpath, self.xpath)

    def test_find_association_energy_elements(self):
        """Test to see if association elements found as expected.
        """
        elements_list = self.element_list
        self.assertEqual(18, len(elements_list))
