#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for testing phase XML element parsing.

@author: mark
"""

import logging
import unittest
import pathlib
import xmlvalidator.xmlvalidation as xmlval
import phasexmlparser.parsers.phasexmlelementparser as phasexmlelementparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PhaseXMLElementParserTestCase(unittest.TestCase):
    """Test case for phase xml element parsing tests.
    """

    def setUp(self):
        """Set up for tests.
        """
        phase_file = (
            (pathlib.Path(__file__).parents[1] / "resources/example_phase.xml")
            .absolute()
            .as_posix()
        )
        mixture_etree = xmlval.validate_and_read_xml_file(
            phase_file, xmlval.PHASE_SCHEMA
        )
        self.phase_element = mixture_etree.xpath(
            "//phase:Phase", namespaces=phasexmlelementparser.PHASE_NAMESPACE_DICT
        )[0]
        self.molecule_element = self.phase_element.xpath(
            "phase:Molecules/phase:Molecule",
            namespaces=phasexmlelementparser.PHASE_NAMESPACE_DICT,
        )[0]

    def tearDown(self):
        """Clean up after tests.
        """
        del self.phase_element
        del self.molecule_element

    def test_parse_phase_concentrations(self):
        """Test to see if expecetd iformation is parsed.
        """
        expected_dict_dict = {
            "phase_type": None,
            "temperature_value": 298.000,
            "temperature_unit": "KELVIN",
            "solvent_id": "water",
            "concentrations": {
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                    "concentration_value": 55.6265913342674,
                    "concentration_unit": "MOLAR",
                }
            },
        }
        actual_dict_dict = phasexmlelementparser.parse_phase_concentrations(
            self.phase_element
        )

        for k, v in expected_dict_dict.items():
            if k == "concentrations":
                actual_dict = actual_dict_dict[k]
                expected_dict = v
                for key, value in expected_dict.items():
                    self.assertTrue(key in actual_dict.keys())
                    for entry_key, entry_value in value.items():
                        if entry_key == "concentration_value":
                            self.assertAlmostEqual(
                                entry_value, actual_dict[key][entry_key]
                            )
                        else:
                            self.assertEqual(entry_value, actual_dict[key][entry_key])
            elif k == "temperature_value":
                self.assertAlmostEqual(v, actual_dict_dict[k])
            else:
                self.assertEqual(v, actual_dict_dict[k])

    def test_get_phase_type(self):
        """Test to see if expected type is returned.
        """
        actual_type = phasexmlelementparser.get_phase_type(self.phase_element)
        self.assertEqual(None, actual_type)

    def test_get_phase_temperature(self):
        """Test to see if expected temperature is returned.
        """
        actual_value, actual_unit = phasexmlelementparser.get_phase_temperature(
            self.phase_element
        )
        self.assertAlmostEqual(298.000, actual_value)
        self.assertEqual("KELVIN", actual_unit)

    def test_get_solvent_id(self):
        """Test to see if expected solvent ID is returned.
        """
        actual_id = phasexmlelementparser.get_solvent_id(self.phase_element)
        self.assertEqual("water", actual_id)

    def test_parse_molecule_concentrations(self):
        """Test to see if expected molecule information is parsed.
        """
        expected_dict = {
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                "concentration_value": 55.6265913342674,
                "concentration_unit": "MOLAR",
            }
        }
        actual_dict = phasexmlelementparser.parse_molecule_concentrations(
            self.phase_element
        )
        for key, value in expected_dict.items():
            self.assertTrue(key in actual_dict.keys())
            for entry_key, entry_value in value.items():
                if entry_key == "concentration_value":
                    self.assertAlmostEqual(entry_value, actual_dict[key][entry_key])
                else:
                    self.assertEqual(entry_value, actual_dict[key][entry_key])

    def test_get_molecule_concentration(self):
        """Test to see if expected concentration and unit are parsed.
        """
        actual_conc, actual_unit = phasexmlelementparser.get_molecule_concentration(
            self.molecule_element
        )
        self.assertEqual("MOLAR", actual_unit)
        self.assertAlmostEqual(55.6265913342674, actual_conc)

    def test_get_molecule_id(self):
        """Test to see if expected molecule ID is returned.
        """
        actual_id = phasexmlelementparser.get_molecule_id(self.molecule_element)
        self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", actual_id)
