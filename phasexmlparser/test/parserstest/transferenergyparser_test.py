#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for testing the transfer energy parsing.

@author: mark
"""

import logging
import unittest
import pathlib
import phasexmlparser.parsers.transferenergyparser as transferenergyparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class TransferEnergyParserTestCase(unittest.TestCase):
    """
    """

    def setUp(self):
        """Set up before tests.
        """
        parent_directory = pathlib.Path(__file__).parents[1]
        self.free_energy_file = (
            (parent_directory / "resources/energyvaluestest.xml").absolute().as_posix()
        )
        self.binding_energy_file = (
            (parent_directory / "resources/energyvaluestest2.xml").absolute().as_posix()
        )

    def tearDown(self):
        """Clean up after tests.
        """

    def test_parse_transfer_binding_energy_info(self):
        """Test to see if expected dict is parsed.
        """
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
        }
        actual_dict_dict = transferenergyparser.parse_transfer_binding_energy_info(
            self.binding_energy_file, "", "water", "MOLEFRACTION"
        )
        self.assertListEqual(["-14.100solute//water"], sorted(actual_dict_dict.keys()))
        actual_dict = actual_dict_dict["-14.100solute//water"]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])

    def test_parse_transfer_free_energy_info(self):
        """Test to see if expected dict is parsed from file.
        """
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
        }
        actual_dict_dict = transferenergyparser.parse_transfer_free_energy_info(
            self.free_energy_file, "", "water", "MOLEFRACTION"
        )
        self.assertListEqual(["-14.100solute//water"], sorted(actual_dict_dict.keys()))
        actual_dict = actual_dict_dict["-14.100solute//water"]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])

