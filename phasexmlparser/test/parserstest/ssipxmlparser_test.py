#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script containing unit tests for value extraction

@author: mark
"""

import unittest
from lxml import etree
import numpy as np
import logging
import pathlib
import phasexmlparser.parsers.ssipxmlparser as ssipextract

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

class SsipXMLParserTestCase(unittest.TestCase):
    """Test case for ssip XML parsing"""
    def setUp(self):
        """Set up before tests."""
        self.maxDiff=None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.ssip_filename = (parent_directory / 'resources/XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml').absolute().as_posix()
        self.ssip_etree = ssipextract.read_ssip_file(self.ssip_filename)
        self.expected_contents = '''<?xml-stylesheet type="text/xsl" href="http://www-hunter.ch.cam.ac.uk/xlst/ssip.xsl"?>
<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="6.0.0" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:formalCharge="0" cml:x3="-0.197781963496" cml:y3="-7.5088692891E-35" cml:z3="0.34665983175"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:formalCharge="0" cml:x3="0.760536914227" cml:y3="-9.28739754019E-37" cml:z3="0.204548524786"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:formalCharge="0" cml:x3="-0.56175495073" cml:y3="5.1742035555E-35" cml:z3="-0.551208356536"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:Surfaces>
            <ssip:Surface>
                <ssip:TotalSurfaceArea ssip:unit="Å^2">37.46449353147367</ssip:TotalSurfaceArea>
                <ssip:NegativeSurfaceArea ssip:unit="Å^2">18.417074471275452</ssip:NegativeSurfaceArea>
                <ssip:PositiveSurfaceArea ssip:unit="Å^2">19.047419060197754</ssip:PositiveSurfaceArea>
                <ssip:ElectronDensityIsosurface ssip:unit="e bohr^-3">0.002</ssip:ElectronDensityIsosurface>
                <ssip:NumberOFMEPSPoints>2267</ssip:NumberOFMEPSPoints>
                <ssip:VdWVolume ssip:unit="Å^3">21.118225673870302</ssip:VdWVolume>
                <ssip:ElectrostaticPotentialMax ssip:unit="kJ mol^-1">222.31155482388002</ssip:ElectrostaticPotentialMax>
                <ssip:ElectrostaticPotentialMin ssip:unit="kJ mol^-1">-185.78297861082</ssip:ElectrostaticPotentialMin>
            </ssip:Surface>
        </ssip:Surfaces>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.8" ssip:nearestAtomID="a2" cml:x3="1.7703677783019898" cml:y3="-0.037716579245226" cml:z3="-0.12404973071057998"/>
        <ssip:SSIP ssip:value="2.8" ssip:nearestAtomID="a3" cml:x3="-0.78007076629588" cml:y3="-0.131868324564555" cml:z3="-1.585252051411308"/>
        <ssip:SSIP ssip:value="-4.5" ssip:nearestAtomID="a1" cml:x3="-0.78007076629588" cml:y3="-1.185660785500926" cml:z3="1.34163366293468"/>
        <ssip:SSIP ssip:value="-4.5" ssip:nearestAtomID="a1" cml:x3="-0.9035034763341259" cml:y3="0.8351649972210169" cml:z3="1.5735514132586688"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>
'''
        self.cml_molecule = '''<cml:molecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:formalCharge="0" cml:x3="-0.197781963496" cml:y3="-7.5088692891E-35" cml:z3="0.34665983175"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:formalCharge="0" cml:x3="0.760536914227" cml:y3="-9.28739754019E-37" cml:z3="0.204548524786"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:formalCharge="0" cml:x3="-0.56175495073" cml:y3="5.1742035555E-35" cml:z3="-0.551208356536"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    
'''
    def tearDown(self):
        """Clean up after tests
        """
        del self.ssip_filename
        del self.ssip_etree
        del self.expected_contents
    def test_parse_ssip_information_from_file(self):
        """Test to see if expected information is parsed.
        """
        expected_array = np.array([2.8,  2.8, -4.5, -4.5 ])
        expected_inchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N"
        expected_surface_info = [{'EP max': 222.31155482388002,
                         'EP min': -185.78297861082,
                         'electron density isosurface': 0.002,
                         'negative surface area': 18.417074471275452,
                         'positive surface area': 19.047419060197754,
                         'total surface area': 37.46449353147367,
                         'volume': 21.118225673870302}]
        ssip_data = ssipextract.parse_ssip_information_from_file(self.ssip_filename)
        self.assertEqual(expected_inchikey, ssip_data["inchikey"])
        np.testing.assert_allclose(expected_array, np.array(ssip_data["ssip values"]))
        self.assertMultiLineEqual(self.cml_molecule, etree.tounicode(ssip_data["cml molecule"], pretty_print=True))
        self.assertListEqual(expected_surface_info, ssip_data["surface info"])
    def test_extract_surface_information(self):
        """Test expected list produced.

        Returns
        -------
        None.

        """
        expected_list = [{'EP max': 222.31155482388002,
                         'EP min': -185.78297861082,
                         'electron density isosurface': 0.002,
                         'negative surface area': 18.417074471275452,
                         'positive surface area': 19.047419060197754,
                         'total surface area': 37.46449353147367,
                         'volume': 21.118225673870302}]
        actual_list = ssipextract.extract_surface_information(self.ssip_etree)
        self.assertListEqual(expected_list, actual_list)
    def test_process_surface(self):
        """Test expected dict produced.

        Returns
        -------
        None.

        """
        expected_dict = {'EP max': 222.31155482388002,
                         'EP min': -185.78297861082,
                         'electron density isosurface': 0.002,
                         'negative surface area': 18.417074471275452,
                         'positive surface area': 19.047419060197754,
                         'total surface area': 37.46449353147367,
                         'volume': 21.118225673870302}
        surface_etree = self.ssip_etree.xpath("ssip:SurfaceInformation/ssip:Surfaces/ssip:Surface",
                                              namespaces=ssipextract.SSIP_NAMESPACE_DICT)[0]
        actual_dict = ssipextract.process_surface(surface_etree)
        self.assertDictEqual(expected_dict, actual_dict)
    def test_extract_ssip_values(self):
        """Test to see if expected values are extracted.
        """
        expected_array = np.array([2.8,  2.8, -4.5, -4.5 ])
        np.testing.assert_allclose(np.array(ssipextract.extract_ssip_values(self.ssip_etree)), expected_array)

    def test_extract_cml_molecule(self):
        """Test to extract cml molecule.
        """
        expected_molecule = self.cml_molecule
        actual_molecule = ssipextract.extract_cml_molecule(self.ssip_etree)
        self.assertMultiLineEqual(expected_molecule, etree.tounicode(actual_molecule, pretty_print=True))
    def test_extract_ssip_inchikey(self):
        """Test to see if expected inchikey is extracted.
        """
        expected_inchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N"
        self.assertEqual(expected_inchikey, ssipextract.extract_ssip_inchikey(self.ssip_etree))
    def test_read_ssip_file(self):
        """Test to see if expected file is read in.
        """
        self.assertMultiLineEqual(self.expected_contents, etree.tounicode(self.ssip_etree, pretty_print=True))
