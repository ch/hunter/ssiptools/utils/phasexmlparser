#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script containing tests for the fractionaloccupancyparser.

@author: mark
"""

import unittest
import logging
import pathlib
from lxml import etree
import xmlvalidator.xmlvalidation as xmlval
import phasexmlparser.parsers.fractionaloccupancyparser as fracparse

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class FractionalOccupancyParserTestCase(unittest.TestCase):
    """Test case for fractionaloccupancyparser."""

    def setUp(self):
        """Test set up."""
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.frac_occ_file = (parent_directory / "resources/fractionaloccupancy.xml").absolute().as_posix()
        self.frac_occ_collection = xmlval.validate_and_read_xml_file(self.frac_occ_file, xmlval.PHASE_SCHEMA)
        self.frac_occ_element_list = fracparse.extract_fractional_occupancies(self.frac_occ_collection)
        self.frac_occ_list = [{"value":0.7380133333333335, 
    "temperature":298.0,
    "solvent_id":"water",
    "solute_id":"XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute"}]
    def tearDown(self):
        """Clean up after tests."""
        del self.frac_occ_file
        del self.frac_occ_collection
        del self.frac_occ_element_list
        del self.frac_occ_list
    def test_parse_fractional_occupancy_file(self):
        """Test File is parsed as expected.

        Returns
        -------
        None.

        """
        expected_condensed_dict = {"water298.000":0.7380133333333335}
        actual_condensed_dict = fracparse.parse_fractional_occupancy_file(self.frac_occ_file)
        self.assertDictEqual(expected_condensed_dict, actual_condensed_dict)
    def test_process_fractional_occupancy_information(self):
        """Test informastion processed to correct format.

        Returns
        -------
        None.

        """
        expected_condensed_dict = {"water298.000":0.7380133333333335}
        actual_condensed_dict = fracparse.process_fractional_occupancy_information(self.frac_occ_list, True)
        self.assertDictEqual(expected_condensed_dict, actual_condensed_dict)
        expected_not_condensed_dict = {"water":self.frac_occ_list}
        actual_not_condensed_dict = fracparse.process_fractional_occupancy_information(self.frac_occ_list, False)
        self.assertDictEqual(expected_not_condensed_dict, actual_not_condensed_dict)
    def test_parse_fractional_occupancy_list(self):
        """Test parse list of fractional occupancies.

        Returns
        -------
        None.

        """
        actual_list = fracparse.parse_fractional_occupancy_list(self.frac_occ_element_list)
        self.assertEqual(1, len(actual_list))
        self.assertDictEqual(self.frac_occ_list[0], actual_list[0])
    def test_extract_fractional_occupancies(self):
        """Test expected number of fractional occupancies where found.

        Returns
        -------
        None.

        """
        self.assertEqual(1, len(self.frac_occ_element_list))
    def test_parse_fractional_occupancy(self):
        """Test expected information extracted.

        Returns
        -------
        None.

        """
        actual_dict = fracparse.parse_fractional_occupancy(self.frac_occ_element_list[0])
        self.assertDictEqual(self.frac_occ_list[0], actual_dict)
    def test_read_and_validate_xml_file(self):
        """Test file read as expected.

        Returns
        -------
        None.

        """
        expected_xml = """<?xml-stylesheet type="text/xsl" href="http://www-hunter.ch.cam.ac.uk/xlst/phase.xsl"?>
<phase:FractionalOccupancyCollection xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd http://www-hunter.ch.cam.ac.uk/PhaseSchema http://www-hunter.ch.cam.ac.uk/schema/PhaseSchema.xsd">
    <phase:FractionalOccupancies>
        <phase:FractionalOccupancy phase:value="0.7380133333333335" phase:solventID="water" phase:soluteID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:temperature="298.0"/>
    </phase:FractionalOccupancies>
</phase:FractionalOccupancyCollection>
"""
        self.assertMultiLineEqual(expected_xml, etree.tounicode(self.frac_occ_collection, pretty_print=True))
        