#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Test case for solventxmlparser.

@author: mark
"""

import logging
import unittest
import os
import pandas
import pathlib
from lxml import etree
import phasexmlparser.parsers.solventxmlparser as solvparser
import xmlvalidator.xmlvalidation as xmlval

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SolventXMLParserTestCase(unittest.TestCase):
    """Test case for solvent XML parser script methods
    """

    def setUp(self):
        """Set up before tests.
        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.solvent_filename = (
            (parent_directory / "resources/watersolvent2.xml").absolute().as_posix()
        )
        self.xml_file = xmlval.validate_and_read_xml_file(self.solvent_filename,
                                                          xmlval.PHASE_SCHEMA)
        self.solvent_xml = self.xml_file.xpath(
            "/phase:SolventList/phase:Solvents/phase:Solvent",
            namespaces=solvparser.PHASE_NAMESPACE_DICT,
        )[0]
        self.molecule_xml = self.solvent_xml.xpath(
            "phase:Molecules/phase:Molecule", namespaces=solvparser.PHASE_NAMESPACE_DICT
        )[0]
        self.actual_filename = "summary.csv"
        self.expected_filename = (
            (parent_directory / "resources/expectedsummary.csv").absolute().as_posix()
        )

    def tearDown(self):
        """Clean up after tests.
        """
        del self.solvent_filename
        del self.xml_file
        del self.solvent_xml
        del self.molecule_xml
        if os.path.isfile(self.actual_filename):
            os.remove(self.actual_filename)
        del self.actual_filename
        del self.expected_filename

    def test_read_and_parse_solvents(self):
        """Test to see if expected file is produced.
        """
        solvparser.read_and_parse_solvents(
            self.solvent_filename, self.actual_filename
        )
        with open(self.expected_filename, "r") as exp_file:
            with open(self.actual_filename, "r") as act_file:
                self.assertMultiLineEqual(exp_file.read(), act_file.read())

    def test_write_to_csv(self):
        """Test to see if file is written out as expected.
        """
        actual_dataframe = solvparser.parse_solvents(self.xml_file)
        solvparser.write_to_csv(actual_dataframe, self.actual_filename)
        with open(self.expected_filename, "r") as exp_file:
            with open(self.actual_filename, "r") as act_file:
                self.assertMultiLineEqual(exp_file.read(), act_file.read())

    def test_parse_solvents(self):
        """Test to see if expected dataframe produced.
        """
        expected_dataframe = pandas.DataFrame(
            data=[
                [
                    "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                    2,
                    2,
                    4,
                    "37.48483410829331",
                    "18.82047542519977",
                    "18.664358683092974",
                    "water",
                ]
            ],
            columns=[
                "inchikey",
                "neg_ssips",
                "pos_ssips",
                "total_ssips",
                "Total Surface Area",
                "Positive Surface Area",
                "Negative Surface Area",
                "Name",
            ],
        )
        actual_dataframe = solvparser.parse_solvents(self.xml_file)
        self.assertMultiLineEqual(
            expected_dataframe.to_string(), actual_dataframe.to_string()
        )

    def test_parse_solvent_information(self):
        """Test to see if expected dataframe is returned.
        """
        expected_dataframe = pandas.DataFrame(
            data=[
                [
                    "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                    2,
                    2,
                    4,
                    "37.48483410829331",
                    "18.82047542519977",
                    "18.664358683092974",
                    "water",
                ]
            ],
            columns=[
                "inchikey",
                "neg_ssips",
                "pos_ssips",
                "total_ssips",
                "Total Surface Area",
                "Positive Surface Area",
                "Negative Surface Area",
                "Name",
            ],
        )
        actual_dataframe = solvparser.parse_solvent_information(self.solvent_xml)
        self.assertMultiLineEqual(
            expected_dataframe.to_string(), actual_dataframe.to_string()
        )

    def test_parse_solvent_name(self):
        """Test to see if solvent name extracted as expected.
        """
        actual_name = solvparser.parse_solvent_name(self.solvent_xml)
        self.assertEqual("water", actual_name)

    def test_parse_molecule(self):
        """Test to see if expected dict is returned.
        """
        expected_dict = {
            "neg_ssips": 2,
            "pos_ssips": 2,
            "total_ssips": 4,
            "Total Surface Area": "37.48483410829331",
            "Positive Surface Area": "18.82047542519977",
            "Negative Surface Area": "18.664358683092974",
            "inchikey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
        }
        actual_dict = solvparser.parse_molecule(self.molecule_xml)
        self.assertDictEqual(expected_dict, actual_dict)

    def test_parse_inchikey(self):
        """Test to see if inchikey extracted as expected
        """
        actual_inchikey = solvparser.parse_inchikey(self.molecule_xml)
        self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", actual_inchikey)

    def test_parse_number_of_ssips_by_category(self):
        """Test to see if expected number is extracted.
        """
        expected_dict = {"neg_ssips": 2, "pos_ssips": 2, "total_ssips": 4}
        actual_dict = solvparser.parse_number_of_ssips_by_category(self.molecule_xml)
        self.assertDictEqual(expected_dict, actual_dict)

    def test_parse_surface_information(self):
        """
        """
        expected_dict = {
            "Total Surface Area": "37.48483410829331",
            "Positive Surface Area": "18.82047542519977",
            "Negative Surface Area": "18.664358683092974",
        }
        actual_dict = solvparser.parse_surface_information(self.molecule_xml)
        self.assertDictEqual(expected_dict, actual_dict)

    def test_read_and_validate_file(self):
        """Test to see if file read as expected.
        """
        expected_xml = """<phase:SolventList xmlns:cml="http://www.xml-cml.org/schema" xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP">
  <phase:Solvents>
    <phase:Solvent phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
      <phase:Molecules>
        <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleFraction="1.000"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.350000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.350000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.350000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.350000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
      </phase:Molecules>
    </phase:Solvent>
  </phase:Solvents>
</phase:SolventList>
"""
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(self.xml_file, pretty_print=True)
        )
