#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script to test partition coefficient parsing script.

@author: mark
"""

import unittest
import logging
import pathlib
from lxml import etree
import xmlvalidator.xmlvalidation as xmlval
import phasexmlparser.parsers.partitioncoefficientparser as partparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PartitionCoefficientParserTestCase(unittest.TestCase):
    """
    """

    def setUp(self):
        """set up before tests.
        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.filename = (
            (parent_directory / "resources/partitionvaluestest.xml")
            .absolute()
            .as_posix()
        )
        self.xmlfile = xmlval.validate_and_read_xml_file(self.filename, xmlval.PHASE_SCHEMA)
        self.xpath_expression = partparser.generate_xpath("water", "water")
        self.partition_element = partparser.parse_partion_coefficents_elements_from_file(
            self.filename, self.xpath_expression
        )[
            0
        ]

    def tearDown(self):
        """Clean up after tests.
        """
        del self.filename
        del self.xmlfile
        del self.xpath_expression
        del self.partition_element

    def test_generate_xpath(self):
        """Test to see if xpath expression is generated as expected.
        """
        expected_xpath = '/phase:EnergyValues/phase:PartitionCoefficientCollection/phase:PartitionCoefficient[@phase:fromSolventID="water"][@phase:toSolventID="water"]'
        self.assertEqual(expected_xpath, self.xpath_expression)

    def test_parse_partition_information_from_file(self):
        """Test to see if information is correctly parsed.
        """
        expected_dict = {
            "total_energy": -0.0,
            "value_type": "MOLAR",
            "to_solvent_id": "water",
            "from_solvent_id": "water",
            "molecule_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute",
        }
        actual_list = partparser.parse_partition_information_from_file(
            self.filename, "water", "water"
        )
        self.assertEqual(1, len(actual_list))
        actual_dict = actual_list[0]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            with self.subTest(key=key):
                if key == "total_energy":
                    self.assertAlmostEqual(value, actual_dict[key])
                else:
                    self.assertEqual(value, actual_dict[key])

    def test_parse_partition_coefficients_from_file(self):
        """Test to see if information is correctly parsed.
        """
        expected_dict = {
            "total_energy": -0.0,
            "value_type": "MOLAR",
            "to_solvent_id": "water",
            "from_solvent_id": "water",
            "molecule_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute",
        }
        actual_list = partparser.parse_partition_coefficients_from_file(
            self.filename, self.xpath_expression
        )
        self.assertEqual(1, len(actual_list))
        actual_dict = actual_list[0]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            with self.subTest(key=key):
                if key == "total_energy":
                    self.assertAlmostEqual(value, actual_dict[key])
                else:
                    self.assertEqual(value, actual_dict[key])

    def test_parse_partition_element_list(self):
        """Test to see if partition coefficient element list is parsed as expected.
        """
        expected_dict = {
            "total_energy": -0.0,
            "value_type": "MOLAR",
            "to_solvent_id": "water",
            "from_solvent_id": "water",
            "molecule_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute",
        }
        actual_list = partparser.parse_partition_element_list([self.partition_element])
        self.assertEqual(1, len(actual_list))
        actual_dict = actual_list[0]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            with self.subTest(key=key):
                if key == "total_energy":
                    self.assertAlmostEqual(value, actual_dict[key])
                else:
                    self.assertEqual(value, actual_dict[key])

    def test_parse_partition_element(self):
        """Test to see if partition element parsed as expected.
        """
        expected_dict = {
            "total_energy": -0.0,
            "value_type": "MOLAR",
            "to_solvent_id": "water",
            "from_solvent_id": "water",
            "molecule_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute",
        }
        actual_dict = partparser.parse_partition_element(self.partition_element)
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            with self.subTest(key=key):
                if key == "total_energy":
                    self.assertAlmostEqual(value, actual_dict[key])
                else:
                    self.assertEqual(value, actual_dict[key])

    def test_get_totalenergy(self):
        """Test to see if expected total energy.
        """
        actual_totalenergy = partparser.get_totalenergy(self.partition_element)
        self.assertAlmostEqual(0.0, actual_totalenergy)

    def test_get_valuetype(self):
        """Test to see if value type is extracted.
        """
        actual_valuetype = partparser.get_valuetype(self.partition_element)
        self.assertEqual("MOLAR", actual_valuetype)

    def test_get_to_solventid(self):
        """Test to see if ID extracted as expected.
        """
        actual_id = partparser.get_to_solventid(self.partition_element)
        self.assertEqual("water", actual_id)

    def test_get_from_solventid(self):
        """Test to see if ID extracted as expected.
        """
        actual_id = partparser.get_from_solventid(self.partition_element)
        self.assertEqual("water", actual_id)

    def test_get_molecule_id(self):
        """Test to see if moleculeID
        """
        actual_id = partparser.get_molecule_id(self.partition_element)
        self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", actual_id)

    def test_parse_partion_coefficents_elements_from_file(self):
        """Test to see if expected number of elements are found.
        """
        self.assertEqual(
            1,
            len(
                partparser.parse_partion_coefficents_elements_from_file(
                    self.filename, self.xpath_expression
                )
            ),
        )

    def test_find_partition_coefficients(self):
        """Test to see if all the partition coefficient entries of interest are found.
        """
        self.assertEqual(
            1,
            len(
                partparser.find_partition_coefficients(
                    self.xmlfile, self.xpath_expression
                )
            ),
        )

    def test_read_and_validate_xml_file(self):
        """Test to see if expected file is read in.
        """
        expected_xml = """<?xml-stylesheet type="text/xsl" href="http://www-hunter.ch.cam.ac.uk/xlst/phase.xsl"?>
<phase:EnergyValues xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd http://www-hunter.ch.cam.ac.uk/PhaseSchema http://www-hunter.ch.cam.ac.uk/schema/PhaseSchema.xsd">
    <phase:PartitionCoefficientCollection>
        <phase:PartitionCoefficient phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:toSolventID="water" phase:fromSolventID="water" phase:valueType="MOLAR">
            <phase:TotalEnergy>-0.0</phase:TotalEnergy>
            <phase:EnergyContributions>
                <phase:EnergyContribution phase:ssipID="1">0.0</phase:EnergyContribution>
                <phase:EnergyContribution phase:ssipID="2">0.0</phase:EnergyContribution>
                <phase:EnergyContribution phase:ssipID="3">0.0</phase:EnergyContribution>
                <phase:EnergyContribution phase:ssipID="4">0.0</phase:EnergyContribution>
            </phase:EnergyContributions>
        </phase:PartitionCoefficient>
    </phase:PartitionCoefficientCollection>
</phase:EnergyValues>
"""
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(self.xmlfile, pretty_print=True)
        )
