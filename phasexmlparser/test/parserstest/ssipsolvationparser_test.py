#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 14 22:22:20 2018

@author: mark
"""

import logging
import unittest
import pathlib
import phasexmlparser.parsers.ssipsolvationparser as ssipsolvationparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SSIPSolvationParserTestCase(unittest.TestCase):
    """Test case for solvation energies.
    """

    def setUp(self):
        """Set up before tests.
        """
        parent_directory = pathlib.Path(__file__).parents[1]
        self.free_energy_file = (
            (parent_directory / "resources/energyvaluestest.xml").absolute().as_posix()
        )
        self.binding_energy_file = (
            (parent_directory / "resources/energyvaluestest2.xml").absolute().as_posix()
        )

    def test_parse_binding_energy_info_with_value(self):
        """Test to see if expected dictionary is returned.
        """
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
            "ssip_value": -14.100,
        }
        actual_dict_dict = ssipsolvationparser.parse_binding_energy_info_with_value(
            self.binding_energy_file
        )
        self.assertListEqual(["-14.100solute//water"], sorted(actual_dict_dict.keys()))
        actual_dict = actual_dict_dict["-14.100solute//water"]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy" or key == "ssip_value":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])

    def test_parse_free_energy_info_with_value(self):
        """Test to see if expected dictionary is returned.
        """
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
            "ssip_value": -14.100,
        }
        actual_dict_dict = ssipsolvationparser.parse_free_energy_info_with_value(
            self.free_energy_file
        )
        self.assertListEqual(["-14.100solute//water"], sorted(actual_dict_dict.keys()))
        actual_dict = actual_dict_dict["-14.100solute//water"]
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy" or key == "ssip_value":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])

    def test_append_ssip_value(self):
        """Test to see if expected dictionary is returned.
        """
        input_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
        }
        expected_dict = {
            "total_energy": -34.8195308084233,
            "value_type": "MOLEFRACTION",
            "to_solvent_id": "water",
            "from_solvent_id": "",
            "molecule_id": "-14.100solute",
            "ssip_value": -14.100,
        }
        actual_dict = ssipsolvationparser.append_ssip_value(input_dict)
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key, value in expected_dict.items():
            if key == "total_energy" or key == "ssip_value":
                self.assertAlmostEqual(value, actual_dict[key])
            else:
                self.assertEqual(value, actual_dict[key])

    def test_extract_ssip_value(self):
        """Test to see if value is parsed as expected.
        """
        actual_value = ssipsolvationparser.extract_ssip_value("-14.100solute")
        self.assertAlmostEqual(-14.100, actual_value)
