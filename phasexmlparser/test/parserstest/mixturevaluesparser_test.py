#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing information from mixture xml.

@author: mark
"""

import logging
import unittest
import pathlib
from lxml import etree
import xmlvalidator.xmlvalidation as xmlval
import phasexmlparser.parsers.mixturevaluesparser as mixvalparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class MixtureValuesParserTestCase(unittest.TestCase):
    """Test case for Mixture XML parsing.
    """

    def setUp(self):
        """Set up before tests.
        """
        self.maxDiff = None
        self.phase_file = (
            (pathlib.Path(__file__).parents[1] / "resources/example_phase.xml")
            .absolute()
            .as_posix()
        )
        self.mixture_etree = xmlval.validate_and_read_xml_file(self.phase_file,
                                                               xmlval.PHASE_SCHEMA)
        self.mixture_element = self.mixture_etree.xpath(
            "//phase:Mixture",
            namespaces=mixvalparser.phasexmlparser.PHASE_NAMESPACE_DICT,
        )[0]

    def tearDown(self):
        """Clean up after tests.
        """
        del self.mixture_etree
        del self.mixture_element

    def test_parse_mixture_file(self):
        """Test to see if mixture container is parsed correctly after read from file.
        """
        mixture_dict = mixvalparser.parse_mixture_file(self.phase_file)
        self.assertListEqual([None], sorted(mixture_dict.keys()))
        actual_result = mixture_dict.get(None)
        self.assertListEqual(["water"], sorted(actual_result.keys()))
        expected_dict_dict = {
            "phase_type": None,
            "temperature_value": 298.000,
            "temperature_unit": "KELVIN",
            "solvent_id": "water",
            "concentrations": {
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                    "concentration_value": 55.6265913342674,
                    "concentration_unit": "MOLAR",
                }
            },
        }
        actual_dict_dict = actual_result.get("water")
        for k, v in expected_dict_dict.items():
            if k == "concentrations":
                actual_dict = actual_dict_dict[k]
                expected_dict = v
                for key, value in expected_dict.items():
                    self.assertTrue(key in actual_dict.keys())
                    for entry_key, entry_value in value.items():
                        if entry_key == "concentration_value":
                            self.assertAlmostEqual(
                                entry_value, actual_dict[key][entry_key]
                            )
                        else:
                            self.assertEqual(entry_value, actual_dict[key][entry_key])
            elif k == "temperature_value":
                self.assertAlmostEqual(v, actual_dict_dict[k])
            else:
                self.assertEqual(v, actual_dict_dict[k])

    def test_parse_mixture_container(self):
        """Test to see if mixture container is parsed correctly.
        """
        mixture_dict = mixvalparser.parse_mixture_container(self.mixture_etree)
        self.assertListEqual([None], sorted(mixture_dict.keys()))
        actual_result = mixture_dict.get(None)
        self.assertListEqual(["water"], sorted(actual_result.keys()))
        expected_dict_dict = {
            "phase_type": None,
            "temperature_value": 298.000,
            "temperature_unit": "KELVIN",
            "solvent_id": "water",
            "concentrations": {
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                    "concentration_value": 55.6265913342674,
                    "concentration_unit": "MOLAR",
                }
            },
        }
        actual_dict_dict = actual_result.get("water")
        for k, v in expected_dict_dict.items():
            if k == "concentrations":
                actual_dict = actual_dict_dict[k]
                expected_dict = v
                for key, value in expected_dict.items():
                    self.assertTrue(key in actual_dict.keys())
                    for entry_key, entry_value in value.items():
                        if entry_key == "concentration_value":
                            self.assertAlmostEqual(
                                entry_value, actual_dict[key][entry_key]
                            )
                        else:
                            self.assertEqual(entry_value, actual_dict[key][entry_key])
            elif k == "temperature_value":
                self.assertAlmostEqual(v, actual_dict_dict[k])
            else:
                self.assertEqual(v, actual_dict_dict[k])

    def test_parse_mixture_element(self):
        """Test to see if expected information is parsed from Mixture element.
        """
        mixture_id, actual_result = mixvalparser.parse_mixture_element(
            self.mixture_element
        )
        self.assertEqual(None, mixture_id)
        self.assertListEqual(["water"], sorted(actual_result.keys()))
        expected_dict_dict = {
            "phase_type": None,
            "temperature_value": 298.000,
            "temperature_unit": "KELVIN",
            "solvent_id": "water",
            "concentrations": {
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                    "concentration_value": 55.6265913342674,
                    "concentration_unit": "MOLAR",
                }
            },
        }
        actual_dict_dict = actual_result.get("water")
        for k, v in expected_dict_dict.items():
            if k == "concentrations":
                actual_dict = actual_dict_dict[k]
                expected_dict = v
                for key, value in expected_dict.items():
                    self.assertTrue(key in actual_dict.keys())
                    for entry_key, entry_value in value.items():
                        if entry_key == "concentration_value":
                            self.assertAlmostEqual(
                                entry_value, actual_dict[key][entry_key]
                            )
                        else:
                            self.assertEqual(entry_value, actual_dict[key][entry_key])
            elif k == "temperature_value":
                self.assertAlmostEqual(v, actual_dict_dict[k])
            else:
                self.assertEqual(v, actual_dict_dict[k])

    def test_parse_phase_concentrations(self):
        """Test to see if phase concentrations are parsed correctly
        """
        actual_result = mixvalparser.parse_phase_concentrations(self.mixture_element)
        self.assertListEqual(["water"], sorted(actual_result.keys()))
        expected_dict_dict = {
            "phase_type": None,
            "temperature_value": 298.000,
            "temperature_unit": "KELVIN",
            "solvent_id": "water",
            "concentrations": {
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                    "concentration_value": 55.6265913342674,
                    "concentration_unit": "MOLAR",
                }
            },
        }
        actual_dict_dict = actual_result.get("water")
        for k, v in expected_dict_dict.items():
            if k == "concentrations":
                actual_dict = actual_dict_dict[k]
                expected_dict = v
                for key, value in expected_dict.items():
                    self.assertTrue(key in actual_dict.keys())
                    for entry_key, entry_value in value.items():
                        if entry_key == "concentration_value":
                            self.assertAlmostEqual(
                                entry_value, actual_dict[key][entry_key]
                            )
                        else:
                            self.assertEqual(entry_value, actual_dict[key][entry_key])
            elif k == "temperature_value":
                self.assertAlmostEqual(v, actual_dict_dict[k])
            else:
                self.assertEqual(v, actual_dict_dict[k])

    def test_get_mixture_id(self):
        """Test to see if expected ID is returned.
        """
        actual_id = mixvalparser.get_mixture_id(self.mixture_etree)
        self.assertEqual(None, actual_id)
