#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for running tests.

@author: mark
"""

import logging
import sys
import unittest
from phasexmlparser.test.parserstest.energyvaluesparsing_test import (
    EnergyParsingValuesTestCase,
)
from phasexmlparser.test.parserstest.solvationenergyparser_test import (
    SolvationEnergyParserTestCase,
)
from phasexmlparser.test.parserstest.transferenergyparser_test import (
    TransferEnergyParserTestCase,
)
from phasexmlparser.test.parserstest.ssipsolvationparser_test import (
    SSIPSolvationParserTestCase,
)
from phasexmlparser.test.parserstest.phasexmlelementparser_test import (
    PhaseXMLElementParserTestCase,
)
from phasexmlparser.test.parserstest.mixturevaluesparser_test import (
    MixtureValuesParserTestCase,
)
from phasexmlparser.test.parserstest.associationenergyparser_test import (
    AssociationEnergyParserTestCase,
)
from phasexmlparser.test.parserstest.partitioncoefficientparser_test import (
    PartitionCoefficientParserTestCase,
)
from phasexmlparser.test.parserstest.solventxmlparser_test import (
    SolventXMLParserTestCase,
)
from phasexmlparser.test.parserstest.ssipxmlparser_test import (
    SsipXMLParserTestCase
)
from phasexmlparser.test.parserstest.fractionaloccupancyparser_test import (
    FractionalOccupancyParserTestCase
)

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

PARSER_TEST_CASES = [
    EnergyParsingValuesTestCase,
    SolvationEnergyParserTestCase,
    TransferEnergyParserTestCase,
    SSIPSolvationParserTestCase,
    PhaseXMLElementParserTestCase,
    MixtureValuesParserTestCase,
    AssociationEnergyParserTestCase,
    PartitionCoefficientParserTestCase,
    SolventXMLParserTestCase,
    SsipXMLParserTestCase,
    FractionalOccupancyParserTestCase,
]


def create_test_suite():
    """Function creates a test suite and then loads all the tests from the
    different test cases.
    """
    LOGGER.info("setting up loader and test suite")
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    for test_case in PARSER_TEST_CASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    return suite


def run_tests():
    """Runs test suite. Exits if there is a failure.

    Returns
    --------
    None
    """
    LOGGER.info("calling test suite method")
    suite = create_test_suite()
    LOGGER.info("running test suite")
    ret = (
        not unittest.TextTestRunner(verbosity=2, stream=sys.stderr)
        .run(suite)
        .wasSuccessful()
    )
    if ret:
        sys.exit(ret)


if __name__ == "__main__":
    run_tests()
