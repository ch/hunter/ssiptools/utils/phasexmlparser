#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Phasexmlparser package level constants.

:Authors:
    Mark Driver <mdd31>

Attributes
----------

SSIP_NAMESPACE_DICT : dict
    Namespace dictionary for SSIP files, including SSIP and CML namespaces.

PHASE_NAMESPACE_DICT : dict
    Namespace dictionary for phase and energy files.

"""

import logging

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

SSIP_NAMESPACE_DICT = {"ssip":"http://www-hunter.ch.cam.ac.uk/SSIP",
                       "cml":"http://www.xml-cml.org/schema"}


PHASE_NAMESPACE_DICT = {"phase": "http://www-hunter.ch.cam.ac.uk/PhaseSchema",
                        "ssip": "http://www-hunter.ch.cam.ac.uk/SSIP",
                        "cml": "http://www.xml-cml.org/schema",}