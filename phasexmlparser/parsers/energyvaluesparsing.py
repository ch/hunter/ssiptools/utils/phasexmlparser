#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Module with methods for reading in Free energies and Binding energies from XML.

parse_free_energy_elements_from_file can be used to extract free energy information
from a file.

parse_binding_energy_elements_from_file can be used to extract binding energy information
from a file.

:Authors:
    Mark Driver <mdd31>

Attributes
----------

FREE_ENERGY_XPATH : str
    XPath expression for Free Energy elements in Phase XML.

BINDING_ENERGY_XPATH : str
    XPath expression for Binding Energy elements in Phase XML.

"""

import logging
import copy
import xmlvalidator.xmlvalidation as xmlvalidation
from phasexmlparser.constants import PHASE_NAMESPACE_DICT

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


FREE_ENERGY_XPATH = "/phase:EnergyValues/phase:FreeEnergyCollection/phase:FreeEnergy"

BINDING_ENERGY_XPATH = (
    "/phase:EnergyValues/phase:BindingEnergyCollection/phase:BindingEnergy"
)


def parse_free_energy_elements_info_from_file(
    filename, xpath_expression=FREE_ENERGY_XPATH
):
    """Parse all informaiton from free energy elements that match expression.

    Parameters
    ----------
    filename : str
        Energy XML filename.
    xpath_expression : str, optional
        Free energy xpath to use. The default is FREE_ENERGY_XPATH.

    Returns
    -------
    dict
        Free energy information.

    """
    free_energy_element_list = parse_energy_elements_from_file(
        filename, xpath_expression
    )
    return parse_energy_element_list(free_energy_element_list)


def parse_binding_energy_elements_info_from_file(
    filename, xpath_expression=BINDING_ENERGY_XPATH
):
    """Parse all informaiton from binding energy elements that match expression.

    Parameters
    ----------
    filename : str
        Energy XML filename.
    xpath_expression : str, optional
        Binding energy xpath to use. The default is BINDING_ENERGY_XPATH.

    Returns
    -------
    dict
        Binding energy information.

    """
    binding_energy_element_list = parse_energy_elements_from_file(
        filename, xpath_expression
    )
    return parse_energy_element_list(binding_energy_element_list)


def create_xpath_expression(
    base_expression, from_solv_id=None, to_solv_id=None, value_type=None
):
    """Generate XPath expression for energy type based on input base expression.
    
    Base expression is the type of energy. Attributes are then used to narrow
    returned selction based on from_solv_id, to_solv_id or value_type.

    Parameters
    ----------
    base_expression : str
        XPATH Energy element expression.
    from_solv_id : str, optional
        from solvent ID. The default is None.
    to_solv_id : str, optional
        to solvent ID. The default is None.
    value_type : str, optional
        value type. The default is None.

    Returns
    -------
    str
        XPath expression with added attributes restrictions, if specified.

    """
    if from_solv_id is None and to_solv_id is None and value_type is None:
        return base_expression

    attribute_block = "["
    if from_solv_id is not None:
        attribute_block += "@phase:fromSolventID='{0}' and ".format(from_solv_id)
    if to_solv_id is not None:
        attribute_block += "@phase:toSolventID='{0}' and ".format(to_solv_id)
    if value_type is not None:
        attribute_block += "@phase:valueType='{0}'".format(value_type)

    attribute_block += "]"
    attribute_block = attribute_block.replace(" and ]", "]")
    return base_expression + attribute_block


def parse_energy_element_list(energy_element_list):
    """Parse list of energy elements.
    
    Returned Dict keys are composite of molecule ID and solvent IDs (to and from).

    Parameters
    ----------
    free_energy_element_list : list of lxml.etree._Element
        Energy elements to process.

    Returns
    -------
    free_energy_information_dict : dict
        Parsed energy information.

    """
    energy_information_dict = {}
    for energy_element in energy_element_list:
        energy_information = parse_energy_element(energy_element)
        key = (
            energy_information["molecule_id"]
            + "/"
            + energy_information["from_solvent_id"]
            + "/"
            + energy_information["to_solvent_id"]
        )
        energy_information_dict[key] = energy_information
    return energy_information_dict



def parse_energy_element(energy_element):
    """Parse Energy element and return key information in dict.

    Parameters
    ----------
    energy_element : lxml.etree._Element
        Input XML element.

    Returns
    -------
    dict
        total energy, value type, to/from solvent IDs and molecule ID.

    """
    total_energy = get_totalenergy(energy_element)
    value_type = get_valuetype(energy_element)
    to_solvent_id = get_to_solventid(energy_element)
    from_solvent_id = get_from_solventid(energy_element)
    molecule_id = get_molecule_id(energy_element)
    return {
        "total_energy": total_energy,
        "value_type": value_type,
        "to_solvent_id": to_solvent_id,
        "from_solvent_id": from_solvent_id,
        "molecule_id": molecule_id,
    }


def get_totalenergy(free_energy_element):
    """Get the energy of transfer for the complete molecule.
    
    This is the sum of contributions from all SSIPs and also 

    Parameters
    ----------
    free_energy_element : lxml.etree._Element
        Input XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.


    Returns
    -------
    float
        energy of transfer for molecule.

    """
    total_energy_list = free_energy_element.xpath(
        "phase:TotalEnergy", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(total_energy_list) == 1:
        return float(total_energy_list[0].text)
    else:
        raise IndexError("wrong number of attributes found")


def get_valuetype(free_energy_element):
    """Get the valueType of the energy.
    
    The value describes the scaling oif the energy, whether it is molefraction
    or molar.

    Parameters
    ----------
    free_energy_element : lxml.etree._Element
        Input XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.


    Returns
    -------
    str
        energy value type.

    """
    value_type_list = free_energy_element.xpath(
        "@phase:valueType", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(value_type_list) == 1:
        return value_type_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def get_to_solventid(free_energy_element):
    """Get the to Solvent ID of the energy element.

    This is the Solvent ID for the phase the molecule is transferred to.

    Parameters
    ----------
    free_energy_element : Tlxml.etree._Element
        Input XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.


    Returns
    -------
    str
        to solvent ID.

    """
    to_solvent_id_list = free_energy_element.xpath(
        "@phase:toSolventID", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(to_solvent_id_list) == 1:
        return to_solvent_id_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def get_from_solventid(free_energy_element):
    """Get the from Solvent ID of the energy element.

    This is the Solvent ID for the phase the molecule is transferred from.

    Parameters
    ----------
    free_energy_element : lxml.etree._Element
        Input XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.


    Returns
    -------
    str
        from Solvent ID.

    """
    from_solvent_id_list = free_energy_element.xpath(
        "@phase:fromSolventID", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(from_solvent_id_list) == 1:
        return from_solvent_id_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def get_molecule_id(free_energy_element):
    """Get the Molecule ID of the  molecule referenced in the Energy Element.
    
    This corresponds to the species being transferred between phases.

    Parameters
    ----------
    free_energy_element : lxml.etree._Element
        Input XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.

    Returns
    -------
    str
        molecule ID.

    """
    molecule_id_list = free_energy_element.xpath(
        "@phase:moleculeID", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(molecule_id_list) == 1:
        return molecule_id_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def parse_energy_elements_from_file(filename, xpath_expression):
    """Parse Energy elements from input file for further processing.

    Parameters
    ----------
    filename : str
        XPath of elements to extract.
    xpath_expression : str
        XPath of elements to extract: Energy XML elements of interest.

    Returns
    -------
    list of lxml.etree._Element
        Array of XPath results: Energy XML elements of interest.

    """
    element_tree = xmlvalidation.validate_and_read_xml_file(filename,
                                                            xmlvalidation.PHASE_SCHEMA)
    return get_energy_elements(element_tree, xpath_expression)

def get_energy_elements(element_tree, xpath_expression):
    """Get Energy elements that match input XPath expression.

    Parameters
    ----------
    element_tree : lxml.etree._Element
        DOM model of information for processing, conataining elements of interest.
    xpath_expression : str
        XPath of elements to extract: Energy XML elements of interest.

    Returns
    -------
    list of lxml.etree._Element
        Array of XPath results.

    """
    return [copy.deepcopy(result) for result in element_tree.xpath(xpath_expression, namespaces=PHASE_NAMESPACE_DICT)]
