#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing information from a phase XML element.

This is designed to be used when parsing Mixture XML, containing information
about phase systems for VLE analysis.

:Authors:
    Mark Driver <mdd31>
"""


import logging
from phasexmlparser.constants import PHASE_NAMESPACE_DICT
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

def parse_phase_concentrations(phase_element):
    """Parse information from XML element.

    Parameters
    ----------
    phase_element : lxml.etree._Element
        phase:Phase element.

    Returns
    -------
    dict
        Molecule concentration, phase type, solvent ID and temperature
        information for phase.

    """
    phase_type = get_phase_type(phase_element)
    temp_value, temp_unit = get_phase_temperature(phase_element)
    solvent_id = get_solvent_id(phase_element)
    molecule_conc_dict = parse_molecule_concentrations(phase_element)
    return {
        "phase_type": phase_type,
        "temperature_value": temp_value,
        "temperature_unit": temp_unit,
        "solvent_id": solvent_id,
        "concentrations": molecule_conc_dict,
    }


def get_phase_type(phase_element):
    """Get phase type from XML.

    Parameters
    ----------
    phase_element : lxml.etree._Element
        phase:Phase element.

    Returns
    -------
    str
        phase type: either CONDENSED or GAS.

    """
    xpath_expression = "@phase:phaseType"
    try:
        return phase_element.xpath(xpath_expression, namespaces=PHASE_NAMESPACE_DICT)[0]
    except IndexError:
        return None


def get_phase_temperature(phase_element):
    """Get temeprature information from phase.

    Parameters
    ----------
    phase_element : lxml.etree._Element
        phase:Phase element.

    Returns
    -------
    temp_value : float
        temeprature.
    temp_unit : str
        temperature unit.

    """
    xpath_expression_value = "phase:Temperature/text()"
    xpath_expression_unit = "phase:Temperature/@phase:units"
    temp_value = float(
        phase_element.xpath(xpath_expression_value, namespaces=PHASE_NAMESPACE_DICT)[0]
    )
    temp_unit = phase_element.xpath(
        xpath_expression_unit, namespaces=PHASE_NAMESPACE_DICT
    )[0]
    return temp_value, temp_unit


def get_solvent_id(phase_element):
    """Get solvent ID for phase.

    Parameters
    ----------
    phase_element : lxml.etree._Element
        phase:Phase element.

    Returns
    -------
    str
        Solvent ID.

    """
    xpath_expression = "@phase:solventID"
    return phase_element.xpath(xpath_expression, namespaces=PHASE_NAMESPACE_DICT)[0]


def parse_molecule_concentrations(phase_element):
    """Parse molecule concentrations from a phase.

    Parameters
    ----------
    phase_element : lxml.etree._Element
        phase:Phasee element.

    Returns
    -------
    molecule_conc_dict : dict
        molecule concentration information.

    """
    molecule_xpath = "phase:Molecules/phase:Molecule"
    molecule_element_list = phase_element.xpath(
        molecule_xpath, namespaces=PHASE_NAMESPACE_DICT
    )
    molecule_conc_dict = {}
    for molecule_element in molecule_element_list:
        mol_id = get_molecule_id(molecule_element)
        value, unit = get_molecule_concentration(molecule_element)
        molecule_conc_dict[mol_id] = {
            "concentration_value": value,
            "concentration_unit": unit,
        }
    return molecule_conc_dict


def get_molecule_concentration(molecule_element):
    """Get concentration from molecule.
    
    This assumes all SSIPs in a molecule have the same concentration.

    Parameters
    ----------
    molecule_element : lxml.etree._Element
        phase:Molecule element.

    Returns
    -------
    value : float
        concentration.
    unit : str
        concentration unit.

    """
    xpath_conc_expression = "phase:SSIPs/phase:SSIP/phase:TotalConcentration/text()"
    xpath_unit_expression = (
        "phase:SSIPs/phase:SSIP/phase:TotalConcentration/@phase:units"
    )
    unit = molecule_element.xpath(
        xpath_unit_expression, namespaces=PHASE_NAMESPACE_DICT
    )[0]
    value = float(
        molecule_element.xpath(xpath_conc_expression, namespaces=PHASE_NAMESPACE_DICT)[
            0
        ]
    )
    return value, unit


def get_molecule_id(molecule_element):
    """Get molecule ID from element.

    Parameters
    ----------
    molecule_element : lxml.etree._Element
        phase:Molecule element.

    Returns
    -------
    str
        Molecule ID.

    """
    xpath_expression = "@phase:moleculeID"
    return molecule_element.xpath(xpath_expression, namespaces=PHASE_NAMESPACE_DICT)[0]
