#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing information from an Phase Molecule.

Chemical name
My original reference number (assuming you have kept that)
negative surface area
positive surface area
number of negative SSIPs
number of positive SSIPs

@author: mark
"""

import logging
import numpy as np
import pandas
import xmlvalidator.xmlvalidation as xmlval
from phasexmlparser.constants import PHASE_NAMESPACE_DICT


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def read_and_parse_solvents(solvent_filename, output_filename):
    """Extract summary infomration for solvents in file, and ouptut to file.

    Parameters
    ----------
    solvent_filename : str
        Solvent XML filename.
    output_filename : str
        Summary information output filename.

    Returns
    -------
    None
        Result of file wirting operation.

    """
    solvent_etree = xmlval.validate_and_read_xml_file(solvent_filename, xmlval.PHASE_SCHEMA)
    solv_info = parse_solvents(solvent_etree)
    return write_to_csv(solv_info, output_filename)


def write_to_csv(information, filename):
    """Write information dataframe to file.

    Parameters
    ----------
    information : pandas.DataFrame
        Solvent molecule information.
    filename : str
        Output filename.

    Returns
    -------
    None
        result of file writig operation.

    """
    return information.to_csv(
        filename,
        sep="\t",
        index=False,
        columns=[
            "Name",
            "inchikey",
            "neg_ssips",
            "pos_ssips",
            "total_ssips",
            "Total Surface Area",
            "Positive Surface Area",
            "Negative Surface Area",
        ],
    )


def parse_solvents(solvent_file_etree):
    """Parse molecule summary information for all solvents.

    Parameters
    ----------
    solvent_file_etree : lxml.etree._Element
        Solvent container XML element.

    Returns
    -------
    total_dataframe : pondas.DataFrame
        Molecule summary information for all solvents.

    """
    solvent_xpath = "/phase:SolventList/phase:Solvents/phase:Solvent"
    solvent_elements = solvent_file_etree.xpath(
        solvent_xpath, namespaces=PHASE_NAMESPACE_DICT
    )
    total_dataframe = parse_solvent_information(solvent_elements[0])
    for solvent_element in solvent_elements[1:]:
        LOGGER.debug("dataframe shape: %s", total_dataframe.shape)
        dataframe = parse_solvent_information(solvent_element)
        total_dataframe = total_dataframe.append(dataframe, sort=True)
    return total_dataframe


def parse_solvent_information(solvent_element):
    """Parse solvent component infomration and assemble into a dataframe.

    Parameters
    ----------
    solvent_element : lxml.etree._Element
        Solvent XML element.

    Returns
    -------
    molecule_information : pondas.DataFrame
        Molecule summary information.

    """
    solvent_name = parse_solvent_name(solvent_element)
    molecule_elements = solvent_element.xpath(
        "phase:Molecules/phase:Molecule", namespaces=PHASE_NAMESPACE_DICT
    )
    info = list(parse_molecule(molecule_elements[0]).items())
    LOGGER.debug(info)
    info.append(("Name", solvent_name))
    LOGGER.debug([inf[1] for inf in info])
    molecule_information = pandas.DataFrame(
        data=[[inf[1] for inf in info]], columns=[inf[0] for inf in info]
    )

    for mol_element in molecule_elements[1:]:
        info = list(parse_molecule(mol_element).items())
        info.append(("Name", solvent_name))
        molecule_information = molecule_information.append(
            pandas.DataFrame(
                data=[[inf[1] for inf in info]], columns=[inf[0] for inf in info]
            )
        )
    return molecule_information


def parse_solvent_name(solvent_element):
    """Parse solvent name.

    Parameters
    ----------
    solvent_element : lxml.etree._Element
        Solvent XML element.

    Returns
    -------
    str
        Solvent Name.

    """
    return solvent_element.xpath("@phase:solventName", namespaces=PHASE_NAMESPACE_DICT)[
        0
    ]


def parse_molecule(molecule_element):
    """Parse summary information from molecule XML.

    Parameters
    ----------
    molecule_element : lxml.etree._Element
        Molecule element.

    Returns
    -------
    dict
        Molecule information.

    """
    return {
        "inchikey": parse_inchikey(molecule_element),
        **parse_number_of_ssips_by_category(molecule_element),
        **parse_surface_information(molecule_element),
    }


def parse_inchikey(molecule_element):
    """Parse molecule InChIKey from XML.

    Parameters
    ----------
    molecule_element : lxml.etree._Element
        Molecule element.

    Returns
    -------
    str
        InChIKey.

    """
    return molecule_element.xpath(
        "@phase:stdInChIKey", namespaces=PHASE_NAMESPACE_DICT
    )[0]


def parse_number_of_ssips_by_category(molecule_element):
    """Parse summary information about number of SSIPs and type in molecule.

    Parameters
    ----------
    molecule_element : lxml.etree._Element
        Molecule element.

    Returns
    -------
    dict
        SSIP number information.

    """
    ssip_value_xpath = "phase:SSIPs/phase:SSIP/@ssip:value"
    ssip_values = molecule_element.xpath(
        ssip_value_xpath, namespaces=PHASE_NAMESPACE_DICT
    )
    value_array = np.array([float(ssip_value) for ssip_value in ssip_values])
    return {
        "neg_ssips": np.sum(value_array < 0.0),
        "pos_ssips": np.sum(value_array > 0.0),
        "total_ssips": len(value_array),
    }


def parse_surface_information(molecule_element):
    """Parse surface area information from molecule.

    Parameters
    ----------
    molecule_element : lxml.etree._Element
        Molecule element.

    Returns
    -------
    dict
        Surface area information.

    """
    xpath_tot_area = "ssip:SurfaceInformation/ssip:TotalSurfaceArea/text()"
    xpath_pos_area = "ssip:SurfaceInformation/ssip:PositiveSurfaceArea/text()"
    xpath_neg_area = "ssip:SurfaceInformation/ssip:NegativeSurfaceArea/text()"
    tot_area = molecule_element.xpath(xpath_tot_area, namespaces=PHASE_NAMESPACE_DICT)[
        0
    ]
    pos_area = molecule_element.xpath(xpath_pos_area, namespaces=PHASE_NAMESPACE_DICT)[
        0
    ]
    neg_area = molecule_element.xpath(xpath_neg_area, namespaces=PHASE_NAMESPACE_DICT)[
        0
    ]
    return {
        "Total Surface Area": tot_area,
        "Positive Surface Area": pos_area,
        "Negative Surface Area": neg_area,
    }
