#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing Fractional Occupancy information.

:Authors:
    Mark Driver <mdd31>
"""

import logging
import xmlvalidator.xmlvalidation as xmlvalidation
from phasexmlparser.constants import PHASE_NAMESPACE_DICT

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)



def parse_fractional_occupancy_file(filename, condense=True):
    """Parse fractional occupancy information from file.

    Parameters
    ----------
    filename : str
        Fractional occupancy XML filename.
    condense : boolean, optional
        Determine if condensed form of information is to be returned. The default is True.

    Returns
    -------
    dict
        Dictionary of parsed information.

    """
    collection_element = xmlvalidation.validate_and_read_xml_file(filename, xmlvalidation.PHASE_SCHEMA)
    frac_occ_element_list = extract_fractional_occupancies(collection_element)
    frac_occ_inf_list = parse_fractional_occupancy_list(frac_occ_element_list)
    return process_fractional_occupancy_information(frac_occ_inf_list, condense)


def process_fractional_occupancy_information(frac_occ_inf_list, condense):
    """Process fractional occupancy information to produced aggregated dictionary.
    
    key : value pair types are determined by condense.
    
    If condense is true then
    the information is condensed to a series of str: float pairs, where key is
    the solvent ID + temperature value, and value is the fractional occupancy value.
    If multiple instances of the same solvent are present, the value is
    checked to see if they are comparable, raises ValueError if not true
    
    If condense is false then the key is the solvent ID and the value is a
    list of all fractional occuapancy dictionaries.

    Parameters
    ----------
    frac_occ_inf_list : list
        list of fractional occupancy information for processing.
    condense : boolean
        Determine if condensed form of information is to be returned.

    Raises
    ------
    ValueError
        Raised if discrepancy for solvent if condense true and duplicate mixtures
        have different fractional occupancy values.

    Returns
    -------
    frac_occ_dict : dict
        fractional occupancy information.

    """
    frac_occ_dict = {}
    for frac_off_inf in frac_occ_inf_list:
        if not condense:
            if frac_off_inf["solvent_id"] not in frac_occ_dict.keys():
                frac_occ_dict[frac_off_inf["solvent_id"]] = [frac_off_inf]
            else:
                frac_occ_dict[frac_off_inf["solvent_id"]].append(frac_off_inf)
        else:
            occ_dict_key = frac_off_inf["solvent_id"] + "{:.3f}".format(frac_off_inf["temperature"])
            if frac_off_inf["solvent_id"] not in frac_occ_dict.keys():
                frac_occ_dict[occ_dict_key] = frac_off_inf["value"]
            elif abs(frac_occ_dict[occ_dict_key] - frac_off_inf["value"])> 1e-6:
                raise ValueError("Discrepancy between occupancy for solvent at the same temperature.", occ_dict_key)
    return frac_occ_dict

def parse_fractional_occupancy_list(frac_occ_element_list):
    """Parse Fractional Occupancy list to dictionary of information.

    Parameters
    ----------
    frac_occ_element_list : list
        Fractional Occupancy elements.

    Returns
    -------
    frac_occ_info_list : list
        List of fractional occupancy information.

    """
    frac_occ_info_list = []
    for frac_occ_element in frac_occ_element_list:
        frac_occ_info_list.append(parse_fractional_occupancy(frac_occ_element))
    return frac_occ_info_list
    

def extract_fractional_occupancies(frac_occ_collection):
    """Extract all fractional occupancy elements present in etree.

    Parameters
    ----------
    frac_occ_collection : lxml.etree._Element
        Fractional occupancy collection XML element.

    Returns
    -------
    list
        List of fractional occupancy elements found.

    """
    xpath_expression = "/phase:FractionalOccupancyCollection/phase:FractionalOccupancies/phase:FractionalOccupancy"
    return frac_occ_collection.xpath(xpath_expression, namespaces=PHASE_NAMESPACE_DICT)
    


def parse_fractional_occupancy(frac_occ_element):
    """Parse information from fractional Occupancy element.

    Parameters
    ----------
    frac_occ_element : lxml.etree._Element
        Fractional Occupancy element.

    Returns
    -------
    dict
        Fractional occupancy information.

    """
    value = float(frac_occ_element.xpath("@phase:value", namespaces=PHASE_NAMESPACE_DICT)[0])
    temperature = float(frac_occ_element.xpath("@phase:temperature", namespaces=PHASE_NAMESPACE_DICT)[0])
    solvent_id = frac_occ_element.xpath("@phase:solventID", namespaces=PHASE_NAMESPACE_DICT)[0]
    solute_id = frac_occ_element.xpath("@phase:soluteID", namespaces=PHASE_NAMESPACE_DICT)[0]
    return {"value":value, 
    "temperature":temperature,
    "solvent_id":solvent_id,
    "solute_id":solute_id}
    