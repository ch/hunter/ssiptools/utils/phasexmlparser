#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing solavation energies for single SSIP solutes.

Solvation binding energies are used for Functional Group
Interaction Profile (FGIP) and solvation free energies are used for 
Solvent Similarity Index (SSI) calculations. 

The molecule ID for these ideal Single SSIP solutes have names of the format:
    "{SSIP Value}solute".

:Authors:
    Mark Driver <mdd31>
"""

import logging
import phasexmlparser.parsers.solvationenergyparser as solvenergyparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def parse_binding_energy_info_with_value(filename):
    """Parse solvation binding energy with solute SSIP value appended

    Parameters
    ----------
    filename : str
        Energy XML filename.

    Returns
    -------
    updated_info : dict
        Solvation binding energy information.

    """
    binding_energy_info = solvenergyparser.parse_solvation_binding_energy_info(filename)
    updated_info = {}
    for key, energy_info in binding_energy_info.items():
        updated_info[key] = append_ssip_value(energy_info)
    return updated_info


def parse_free_energy_info_with_value(filename):
    """Parse solvation free energy with solute SSIP value appended.

    Parameters
    ----------
    filename : str
        Energy XML filename.

    Returns
    -------
    updated_info : dict
        Solvation free energy information.

    """
    free_energy_info = solvenergyparser.parse_solvation_free_energy_info(filename)
    updated_info = {}
    for key, energy_info in free_energy_info.items():
        updated_info[key] = append_ssip_value(energy_info)
    return updated_info


def append_ssip_value(info_dict):
    """Append SSIP value to solvation energy information.

    Parameters
    ----------
    info_dict : dict
        Solvation energy information dictionary.

    Returns
    -------
    info_dict : dict
        Solvation energy information dictionary.

    """
    ssip_value = extract_ssip_value(info_dict["molecule_id"])
    info_dict["ssip_value"] = ssip_value
    return info_dict


def extract_ssip_value(molecule_id):
    """Extract SSIP value from the molecule ID.

    Parameters
    ----------
    molecule_id : str
        Molecule ID for solvation energy.

    Returns
    -------
    float
        SSIP value.

    """
    return float(molecule_id.replace("solute", ""))
