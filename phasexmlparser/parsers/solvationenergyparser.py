#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing Solvation energies, filtering them from file.

This provides information on only a subset of possible information held within
an Energy XML file. Solvation binding energies are used for Functional Group
Interaction Profile (FGIP) and solvation free energies are used for 
Solvent Similarity Index (SSI) calculations.

:Authors:
    Mark Driver <mdd31>
"""

import logging
import phasexmlparser.parsers.energyvaluesparsing as energyvaluesparsing

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def parse_solvation_binding_energy_info(filename):
    """Parse solvation binding energy information from file only.
    
    Solvation energies have a from_solv_id = "".

    Parameters
    ----------
    filename : str
        Energy XML filename.

    Returns
    -------
    dict
        Binding energy information.

    """
    xpath_expression = energyvaluesparsing.create_xpath_expression(
        energyvaluesparsing.BINDING_ENERGY_XPATH, from_solv_id=""
    )
    return energyvaluesparsing.parse_binding_energy_elements_info_from_file(
        filename, xpath_expression=xpath_expression
    )


def parse_solvation_free_energy_info(filename):
    """Parse Solvation free energy information from file only.
    
    Solvation energies have a from_solv_id = "".

    Parameters
    ----------
    filename : str
        Energy XML filename.

    Returns
    -------
    dict
        Free energy information.

    """
    xpath_expression = energyvaluesparsing.create_xpath_expression(
        energyvaluesparsing.FREE_ENERGY_XPATH, from_solv_id=""
    )
    return energyvaluesparsing.parse_free_energy_elements_info_from_file(
        filename, xpath_expression=xpath_expression
    )
