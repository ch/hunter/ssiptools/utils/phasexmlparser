#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing information from mixture elements from file.

This is designed to extract Phase information for further analysis of VLE data.

:Authors:
    Mark Driver <mdd31>
"""

import logging
import xmlvalidator.xmlvalidation as xmlvalidation
import phasexmlparser.parsers.phasexmlelementparser as phasexmlparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def parse_mixture_file(filename):
    """Parse Mixture container XML file.

    Parameters
    ----------
    filename : str
        XML filename.

    Returns
    -------
    dict of dicts
        information of phases in mixture collection.

    """
    mixture_xml = xmlvalidation.validate_and_read_xml_file(filename,
                                                           xmlvalidation.PHASE_SCHEMA)
    return parse_mixture_container(mixture_xml)


def parse_mixture_container(mixture_container_element):
    """Parse mixture container element.

    Parameters
    ----------
    mixture_container_element : lxml.etree._Element
        Mixture container XML element.

    Returns
    -------
    mixture_info : dict
        Dictionary of mixture information, with  mixture ID used as key.

    """
    xpath_expression = "phase:Mixtures/phase:Mixture"
    mixture_element_list = mixture_container_element.xpath(
        xpath_expression, namespaces=phasexmlparser.PHASE_NAMESPACE_DICT
    )
    mixture_info = {}
    for mixture_element in mixture_element_list:
        mixture_id, phase_conc_dict = parse_mixture_element(mixture_element)
        mixture_info[mixture_id] = phase_conc_dict
    return mixture_info


def parse_mixture_element(mixture_element):
    """Parse Mixture element, extracting phase information and mixture ID

    Parameters
    ----------
    mixture_element : lxml.etree._Element
        Mixture XML element.

    Returns
    -------
    mixture_id : TYPE
        DESCRIPTION.
    phase_conc_dict : TYPE
        DESCRIPTION.

    """
    mixture_id = get_mixture_id(mixture_element)
    phase_conc_dict = parse_phase_concentrations(mixture_element)
    return mixture_id, phase_conc_dict


def parse_phase_concentrations(mixture_element):
    """Parse phase concentrations from mixture

    Parameters
    ----------
    mixture_element : lxml.etree._Element
        Mixture XML element.

    Returns
    -------
    phase_conc_dict : dict
        Phase information.

    """
    phase_xpath = "phase:Phases/phase:Phase"
    phase_element_list = mixture_element.xpath(
        phase_xpath, namespaces=phasexmlparser.PHASE_NAMESPACE_DICT
    )
    phase_conc_dict = {}
    for phase_element in phase_element_list:
        phase_conc_info = phasexmlparser.parse_phase_concentrations(phase_element)
        if phase_conc_info.get("phase_type") is not None:
            key = phase_conc_info.get("solvent_id") + phase_conc_info.get("phase_type")
            phase_conc_dict[key] = phase_conc_info
        else:
            phase_conc_dict[phase_conc_info.get("solvent_id")] = phase_conc_info
    return phase_conc_dict


def get_mixture_id(mixture_element):
    """Get the mixture ID.

    Parameters
    ----------
    mixture_element : lxml.etree._Element
        Mixture XML element.

    Returns
    -------
    str
        Mixture ID element.

    """
    xpath_expression = "@phase:mixtureID"
    try:
        return mixture_element.xpath(
            xpath_expression, namespaces=phasexmlparser.PHASE_NAMESPACE_DICT
        )[0]
    except IndexError:
        return None

