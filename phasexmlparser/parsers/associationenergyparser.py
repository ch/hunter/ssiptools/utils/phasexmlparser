#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing association energy values.

This porovides methods to assemble the Association energies into data matrices.
Association information is of interest for Fuctional group interaction profile
(FGIP)

:Authors:
    Mark Driver <mdd31>

"""

import logging
import numpy as np
import xmlvalidator.xmlvalidation as xmlvalidation
from phasexmlparser.constants import PHASE_NAMESPACE_DICT

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def generate_data_matrices(association_data_dict):
    """Generate x, y, z data arrays of association energies.
    
    This is designed to convert infomration into a format for easy plotting of
    association data between two single SSIP sollutes. x, y are solute SSIP values, z is association energy.

    Parameters
    ----------
    association_data_dict : dict
        Association energy information organised by solute.

    Returns
    -------
    dict
        data value arrays.

    """
    x_data = []
    y_data = []
    energy_data = []
    for x_value in sorted([float(x) for x in association_data_dict.keys()]):
        x_value_list = []
        y_value_list = []
        energy_value_list = []
        for y_value in sorted(
            [float(x) for x in association_data_dict["{:.3f}".format(x_value)].keys()]
        ):
            x_value_list.append(x_value)
            y_value_list.append(y_value)
            energy_value_list.append(
                association_data_dict["{:.3f}".format(x_value)][
                    "{:.3f}".format(y_value)
                ]["value"]
            )
        x_data.append(x_value_list)
        y_data.append(y_value_list)
        energy_data.append(energy_value_list)
    return {
        "x_data": np.array(x_data),
        "y_data": np.array(y_data),
        "z_data": np.array(energy_data),
    }


def restructure_data(association_energies):
    """Restructure association data to be nested dictionaries instead of list.

    This is to allow manipulation to be able to plot the information.
    
    Parameters
    ----------
    association_energies : list
        Association energy information.

    Returns
    -------
    solute_info : dict
        Association energy information organised by solute.

    """
    solute_info = {}
    LOGGER.info(association_energies)
    for energy_dict in association_energies:
        LOGGER.info(solute_info)
        LOGGER.debug("energy_dict:")
        LOGGER.debug(energy_dict)
        if energy_dict["solute1"].replace("solute1", "") not in solute_info.keys():
            solute_info[energy_dict["solute1"].replace("solute1", "")] = {
                energy_dict["solute2"].replace("solute2", ""): energy_dict
            }
        else:
            solute_info[energy_dict["solute1"].replace("solute1", "")][
                energy_dict["solute2"].replace("solute2", "")
            ] = energy_dict
    return solute_info


def parse_elements_from_file(filename, solvent_id):
    """Parse association energies from file for given solvent ID.

    Parameters
    ----------
    filename : str
        Association energy filename.
    solvent_id : str
        Solvent ID.

    Returns
    -------
    list
        Association energy information.

    """
    element_tree = xmlvalidation.validate_and_read_xml_file(filename,
                                                            xmlvalidation.PHASE_SCHEMA)
    xpath_expression = generate_association_element_xpath()
    association_element_list = find_association_energy_elements(
        element_tree, xpath_expression
    )
    return sift_association_information(
        parse_association_elements(association_element_list), solvent_id
    )


def sift_association_information(association_energy_information, solvent_id):
    """Filter association energy information, extract values corresponding to solvent ID.

    Parameters
    ----------
    association_energy_information : list
        association energy information.
    solvent_id : str
        solvent ID.

    Returns
    -------
    solute_association_info : list
        Association energy information for given solvent ID.

    """
    solute_association_info = []
    for association_energy_datum in association_energy_information:
        if (
            "solute1" in association_energy_datum["solute1"]
            and "solute2" in association_energy_datum["solute2"]
        ):
            if solvent_id is not None:
                if solvent_id == association_energy_datum["solvent_id"]:
                    solute_association_info.append(association_energy_datum)
            else:
                solute_association_info.append(association_energy_datum)
    return solute_association_info


def parse_association_elements(association_energy_element_list):
    """Parse association energy elements information to list.

    Parameters
    ----------
    association_energy_element_list : list of lxml.etree._Element
        Association eenrgy XML elements.

    Returns
    -------
    association_energy_information : list
        Association energy information.

    """
    association_energy_information = []
    for association_energy_element in association_energy_element_list:
        association_energy_information.append(
            parse_energy_information(association_energy_element)
        )
    return association_energy_information


def parse_energy_information(association_energy_element):
    """Parse association energy infomration from element.

    Parameters
    ----------
    association_energy_element : lxml.etree._Element
        Association Energy element.

    Returns
    -------
    dict
        Association energy information.

    """
    solvent_id, temp = parse_solvent_id(get_solvent_id(association_energy_element))
    solute1 = get_solute1(association_energy_element)
    solute2 = get_solute2(association_energy_element)
    value = get_energy_value(association_energy_element)
    return {
        "solvent_id": solvent_id,
        "solute1": solute1,
        "solute2": solute2,
        "value": value,
        "temp": temp,
    }


def parse_solvent_id(solvent_attribute_string):
    """Parse Solvent ID string to get solvent ID and temperature.

    Parameters
    ----------
    solvent_attribute_string : str
        solvent ID string.

    Returns
    -------
    solvent_id : str
        solvent ID.
    temp : str
        temperature.

    """
    solvent_id, solute1, solute2temp = solvent_attribute_string.split("_")
    temp = solute2temp.split("solute2")[1]
    return solvent_id, temp


def get_solvent_id(association_energy_element):
    """Get Solvent ID for phase of association energy.

    Parameters
    ----------
    association_energy_element : lxml.etree._Element
        Association Energy element.

    Raises
    ------
    IndexError
        Raises error if more or no solvent IDs are found.

    Returns
    -------
    str
        solvent ID.

    """
    solvent_id_list = association_energy_element.xpath(
        "@phase:solventID", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(solvent_id_list) == 1:
        return solvent_id_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def get_energy_value(association_energy_element):
    """Get Energy value from XML element.

    Parameters
    ----------
    association_energy_element : lxml.etree._Element
        Association Energy element.

    Raises
    ------
    IndexError
        Raised if multiple or no values are found.

    Returns
    -------
    flaot
        Associuation energy value.

    """
    value_list = association_energy_element.xpath(
        "@phase:value", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(value_list) == 1:
        return float(value_list[0])
    else:
        raise IndexError("wrong number of attributes found")


def get_solute2(association_energy_element):
    """Get solute 2 molecule ID.
    
    This is the molecule ID for the second species in the complex.

    Parameters
    ----------
    association_energy_element : lxml.etree._Element
        Association Energy element.

    Raises
    ------
    IndexError
        Raised if multiple or no molecule ID 2 are found.

    Returns
    -------
    str
        Solute 2 ID.

    """
    solute_list = association_energy_element.xpath(
        "@phase:moleculeID2", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(solute_list) == 1:
        return solute_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def get_solute1(association_energy_element):
    """Get solute 1 molecule ID.
    
    This is the molecule ID for the first species in the complex.

    Parameters
    ----------
    association_energy_element : lxml.etree._Element
        Association Energy element.

    Raises
    ------
    IndexError
        Raised if multiple or no molecule ID 1 are found.

    Returns
    -------
    str
        Solute 1 ID.

    """
    solute_list = association_energy_element.xpath(
        "@phase:moleculeID1", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(solute_list) == 1:
        return solute_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def generate_association_element_xpath():
    """Generate the XPath expression.

    Returns
    -------
    str
        XPath expression for association energy values.

    """
    return "/phase:AssociationEnergyValueCollection/phase:AssociationEnergyValue"


def find_association_energy_elements(element_tree, xpath_expression):
    """Find association energy elements that match XPAth expression.

    Parameters
    ----------
    element_tree : lxml.etree._Element
        Association Energy collection element.
    xpath_expression : str
        XPath expression for elements.

    Returns
    -------
    list
        List of matching XML elements.

    """
    return element_tree.xpath(xpath_expression, namespaces=PHASE_NAMESPACE_DICT)
