#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing transfer free energies, filtering out any irrelevant entries.

This is done using the XPath epxression constructed to seach for binding or
free energy XML elements. 

:Authors:
    Mark Driver <mdd31>
"""

import logging
import phasexmlparser.parsers.energyvaluesparsing as envalparsing

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def parse_transfer_binding_energy_info(filename, fromsolventid=None,
                                       tosolventid=None, valuetype=None):
    """Parse binding energies of specified solvent/solute compositions.

    Parameters
    ----------
    filename : str
        Energy XML filename.
    from_solv_id : str, optional
        from solvent ID. The default is None.
    to_solv_id : str, optional
        to solvent ID. The default is None.
    value_type : str, optional
        value type. The default is None.

    Returns
    -------
    binding_info : dict
        Binding energy information for phases of interest.

    See Also
    --------
    phasexmlparser.parsers.energyvaluesparsing.create_xpath_expression

    """
    xpath_exp = envalparsing.create_xpath_expression(envalparsing.BINDING_ENERGY_XPATH,
                                                     from_solv_id=fromsolventid,
                                                     to_solv_id=tosolventid,
                                                     value_type=valuetype)
    binding_info = envalparsing.parse_binding_energy_elements_info_from_file(filename, xpath_exp)
    return binding_info


def parse_transfer_free_energy_info(filename, fromsolventid=None,
                                    tosolventid=None, valuetype=None):
    """Parse free energies of specified solvent/solute compositions.

    Parameters
    ----------
    filename : str
        Energy XML filename.
    from_solv_id : str, optional
        from solvent ID. The default is None.
    to_solv_id : str, optional
        to solvent ID. The default is None.
    value_type : str, optional
        value type. The default is None.

    Returns
    -------
    free_energy_info : dict
        Free energy information for phases of interest.

    See Also
    --------
    phasexmlparser.parsers.energyvaluesparsing.create_xpath_expression

    """
    xpath_exp = envalparsing.create_xpath_expression(envalparsing.FREE_ENERGY_XPATH,
                                                     from_solv_id=fromsolventid,
                                                     to_solv_id=tosolventid,
                                                     value_type=valuetype)
    free_energy_info = envalparsing.parse_free_energy_elements_info_from_file(filename, xpath_exp)
    return free_energy_info


