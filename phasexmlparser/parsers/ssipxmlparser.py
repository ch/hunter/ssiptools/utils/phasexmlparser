#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Collection of methods for extracting information from an SSIP file.

parse_ssip_information_from_file is designed to extract relevent information
from an SSIP file for further analysis.

:Authors:
    Mark Driver <mdd31>

Attributes
----------
SSIP_MOL_INCHIKEY_XPATH : str
    XPath expression for InChIKey.

CML_MOL_PATH : str
    XPath expression for cml:molecule element.

SSIP_VALUE_XPATH : str
    XPath expression for SSIP value attribute.

"""

from lxml import etree
import logging
import copy
import xmlvalidator.xmlvalidation as xmlvalidation
from phasexmlparser.constants import SSIP_NAMESPACE_DICT

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


SSIP_MOL_INCHIKEY_XPATH = '/ssip:SSIPMolecule/cml:molecule/@ssip:stdInChIKey'

CML_MOL_PATH = '/ssip:SSIPMolecule/cml:molecule'

SSIP_VALUE_XPATH = '/ssip:SSIPMolecule/ssip:SSIPs/ssip:SSIP/@ssip:value'

def parse_ssip_information_from_file(filename):
    """Parse the SSIP information in an SSIP file.

    Parameters
    ----------
    filename : str
        SSIP filename.

    Returns
    -------
    dict
        SSIP molecule information.

    """
    ssip_mol_etree = read_ssip_file(filename)
    inchikey = extract_ssip_inchikey(ssip_mol_etree)
    cml_molecule = extract_cml_molecule(ssip_mol_etree)
    surface_info = extract_surface_information(ssip_mol_etree)
    ssip_values = extract_ssip_values(ssip_mol_etree)
    return {"inchikey": inchikey,
            "cml molecule": cml_molecule,
            "surface info": surface_info,
            "ssip values": ssip_values}

def extract_surface_information(ssip_mol_etree):
    """Extract surface information from molecule as list.

    Parameters
    ----------
    ssip_mol_etree : lxml.Etree
        Etree representation of SSIPMolecule element.

    Returns
    -------
    surface_info : list
        List of Surface information dictionaries.

    """
    surface_xpath = "ssip:SurfaceInformation/ssip:Surfaces/ssip:Surface"
    surfaces_list = ssip_mol_etree.xpath(surface_xpath, namespaces=SSIP_NAMESPACE_DICT)
    surface_info = []
    for surface in surfaces_list:
        surface_info.append(process_surface(surface))
    return surface_info

def process_surface(surface_etree):
    """Process and extract information relating to the surface used.

    Parameters
    ----------
    surface_etree : lxml.Etree
        Etree representation of Surface element.

    Returns
    -------
    dict
        Surface property values.

    """
    total_surface_area = float(surface_etree.xpath("ssip:TotalSurfaceArea/text()", namespaces=SSIP_NAMESPACE_DICT)[0])
    negative_surface_area = float(surface_etree.xpath("ssip:NegativeSurfaceArea/text()", namespaces=SSIP_NAMESPACE_DICT)[0])
    positive_surface_area =float(surface_etree.xpath("ssip:PositiveSurfaceArea/text()", namespaces=SSIP_NAMESPACE_DICT)[0])
    isosurface = float(surface_etree.xpath("ssip:ElectronDensityIsosurface/text()", namespaces=SSIP_NAMESPACE_DICT)[0])
    volume = float(surface_etree.xpath("ssip:VdWVolume/text()", namespaces=SSIP_NAMESPACE_DICT)[0])
    ep_max = float(surface_etree.xpath("ssip:ElectrostaticPotentialMax/text()", namespaces=SSIP_NAMESPACE_DICT)[0])
    ep_min = float(surface_etree.xpath("ssip:ElectrostaticPotentialMin/text()", namespaces=SSIP_NAMESPACE_DICT)[0])
    return {"total surface area": total_surface_area,
            "negative surface area": negative_surface_area,
            "positive surface area": positive_surface_area,
            "electron density isosurface": isosurface,
            "volume": volume,
            "EP max":ep_max,
            "EP min":ep_min,
            }

def extract_ssip_values(ssip_mol_etree):
    """Extract SSIP values from XML.

    Parameters
    ----------
    ssip_mol_etree : lxml.Etree
        Etree representation of SSIP XML.

    Returns
    -------
    list
        SSIP values.

    """
    ssip_values = ssip_mol_etree.xpath(SSIP_VALUE_XPATH, namespaces=SSIP_NAMESPACE_DICT)
    return [float(x) for x in ssip_values]

def extract_cml_molecule(ssip_mol_etree):
    """ Extract CML molecule from SSIP XML.

    Parameters
    ----------
    ssip_mol_etree : lxml.Etree
        Etree representation of SSIP XML.

    Returns
    -------
    lxml.etree
        Etree of molecule CML.

    """
    cml_molecules = ssip_mol_etree.xpath("//cml:molecule", namespaces=SSIP_NAMESPACE_DICT,)
    LOGGER.debug(f"{cml_molecules}")
    LOGGER.debug(f"{etree.tounicode(cml_molecules[0],pretty_print=True)}")
    return copy.deepcopy(cml_molecules[0])


def extract_ssip_inchikey(ssip_mol_etree):
    """Extract InChIKey value from SSIP XML.

    Parameters
    ----------
    ssip_mol_etree : lxml.Etree
        SSIP molecule Etree.

    Returns
    -------
    str
        InChIKey.

    """
    inchikeys = ssip_mol_etree.xpath(SSIP_MOL_INCHIKEY_XPATH, namespaces=SSIP_NAMESPACE_DICT)
    return inchikeys[0]

def read_ssip_file(filename):
    """Read SSIP file to etree.

    Parameters
    ----------
    filename : str
        SSIP XML filename.

    Returns
    -------
    lxml.Etree
        Etree representation of SSIP XML.

    """
    return xmlvalidation.validate_and_read_xml_file(filename, xmlvalidation.SSIP_SCHEMA)
