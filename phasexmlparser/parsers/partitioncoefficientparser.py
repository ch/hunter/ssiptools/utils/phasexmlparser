#    phasexmlparser provides methods for parsing phase XML for further analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlparser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing partition coefficients.

:Authors:
    Mark Driver <mdd31>

Attributes
----------

PARTITION_ELEMENT_XPATH : str
    Base XPath exression for a partition coefficent element.
"""

import logging
import xmlvalidator.xmlvalidation as xmlvalidation
from phasexmlparser.constants import PHASE_NAMESPACE_DICT

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)



PARTITION_ELEMENT_XPATH = "/phase:EnergyValues/phase:PartitionCoefficientCollection/phase:PartitionCoefficient"


def parse_partition_information_from_file(filename, from_solvent_id, to_solvent_id):
    """Extract all partition coefficient information for corresponding solvents.

    Parameters
    ----------
    filename : str
        partition coefficent XML filename.
    from_solvent_id : str
        From solvent ID.
    to_solvent_id : str
        To solvent ID.

    Returns
    -------
    list of dict
        List of partition coefficent information.

    """
    xpath_expression = generate_xpath(from_solvent_id, to_solvent_id)
    return parse_partition_coefficients_from_file(filename, xpath_expression)


def generate_xpath(from_solvent_id, to_solvent_id):
    """Generate XPath expression for partition coefficients of interest.
    
    Select only elements with to and from solvent ID specified.

    Parameters
    ----------
    from_solvent_id : str
        From solvent ID.
    to_solvent_id : str
        To solvent ID.

    Returns
    -------
    str
        XPath expression for element selection.

    """
    attribute_block = '[@phase:fromSolventID="{0}"][@phase:toSolventID="{1}"]'.format(
        from_solvent_id, to_solvent_id
    )
    return PARTITION_ELEMENT_XPATH + attribute_block


def parse_partition_coefficients_from_file(filename, xpath_expression):
    """Parse partition information from file.

    Parameters
    ----------
    filename : str
        partition coefficent XML filename.
    xpath_expression : str
        XPath expression for partition coefficient XML elements of interest.

    Returns
    -------
    list of dict
        List of partition coefficent information.

    """
    partition_element_list = parse_partion_coefficents_elements_from_file(
        filename, xpath_expression
    )
    return parse_partition_element_list(partition_element_list)


def parse_partition_element_list(partition_element_list):
    """Parse list of partition elements and extract information to list.

    Parameters
    ----------
    partition_element_list : list
        List of partition coefficient XML elements.

    Returns
    -------
    partition_info_list : list of dict
        List of partition coefficent information.

    """
    partition_info_list = []
    for partition_element in partition_element_list:
        partition_info_list.append(parse_partition_element(partition_element))
    return partition_info_list


def parse_partition_element(partition_element):
    """Parse information from partition coefficient element.

    Parameters
    ----------
    partition_element : lxml.etree._Element
        Partition coefficient XML element.

    Returns
    -------
    dict
        total energy, value type, to/from solvent IDs and molecule ID.

    """
    total_energy = get_totalenergy(partition_element)
    value_type = get_valuetype(partition_element)
    to_solvent_id = get_to_solventid(partition_element)
    from_solvent_id = get_from_solventid(partition_element)
    molecule_id = get_molecule_id(partition_element)
    return {
        "total_energy": total_energy,
        "value_type": value_type,
        "to_solvent_id": to_solvent_id,
        "from_solvent_id": from_solvent_id,
        "molecule_id": molecule_id,
    }


def get_totalenergy(partition_element):
    """Get total energy from partition coefficient element.
    
    This is equal to the log of the paprtition coefficient for the molecule.

    Parameters
    ----------
    partition_element : lxml.etree._Element
        Partition coefficient XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.

    Returns
    -------
    float
        partition coefficient value.

    """
    total_energy_list = partition_element.xpath(
        "phase:TotalEnergy", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(total_energy_list) == 1:
        return float(total_energy_list[0].text)
    else:
        raise IndexError("wrong number of attributes found")


def get_valuetype(partition_element):
    """Get valueType for partition coefficient.
    
    This is either MOLAR or MOLEFRACTION

    Parameters
    ----------
    partition_element : lxml.etree._Element
        Partition coefficient XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.

    Returns
    -------
    str
        value type.

    """
    value_type_list = partition_element.xpath(
        "@phase:valueType", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(value_type_list) == 1:
        return value_type_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def get_to_solventid(partition_element):
    """Get to solvent ID.
    
    This is the solvent ID the molecule is transferred to.

    Parameters
    ----------
    partition_element : lxml.etree._Element
        Partition coefficient XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.

    Returns
    -------
    str
        To solvent ID.

    """
    to_solvent_id_list = partition_element.xpath(
        "@phase:toSolventID", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(to_solvent_id_list) == 1:
        return to_solvent_id_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def get_from_solventid(partition_element):
    """Get from solvent ID.
    
    This is the solvent ID the molecule is transferred from.

    Parameters
    ----------
    partition_element : lxml.etree._Element
        Partition coefficient XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.

    Returns
    -------
    str
        From Solvent ID.

    """
    from_solvent_id_list = partition_element.xpath(
        "@phase:fromSolventID", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(from_solvent_id_list) == 1:
        return from_solvent_id_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def get_molecule_id(partition_element):
    """Get molecule ID from partition element.

    Parameters
    ----------
    partition_element : lxml.etree._Element
        Partition coefficient XML element.

    Raises
    ------
    IndexError
        Raised if none or multiple entries are found.

    Returns
    -------
    str
        molecule ID.

    """
    molecule_id_list = partition_element.xpath(
        "@phase:moleculeID", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(molecule_id_list) == 1:
        return molecule_id_list[0]
    else:
        raise IndexError("wrong number of attributes found")


def parse_partion_coefficents_elements_from_file(filename, xpath_expression):
    """Parse partition elements from file.

    Parameters
    ----------
    filename : str
        partition coefficent XML filename.
    xpath_expression : TYPE
        DESCRIPTION.

    Returns
    -------
    list of lxml.etree._Element
        Array of XPath results.

    """
    element_tree = xmlvalidation.validate_and_read_xml_file(filename, xmlvalidation.PHASE_SCHEMA)
    return find_partition_coefficients(element_tree, xpath_expression)


def find_partition_coefficients(element_etree, xpath_expression):
    """Find Pasrtition coefficent elements in etree.

    Parameters
    ----------
    element_etree : lxml.etree._Element
        DOM model of information for processing, conataining elements of interest.
    xpath_expression : str
        XPath of elements to extract: Partition Coefficent XML elements of interest.

    Returns
    -------
    list of lxml.etree._Element
        Array of XPath results.


    """
    return element_etree.xpath(xpath_expression, namespaces=PHASE_NAMESPACE_DICT)

