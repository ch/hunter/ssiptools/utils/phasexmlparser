# PhaseXMLParser #

PhaseXMLParser contains a collection of modules with the ability to parse information from phase, solvent, or energy XML.
These are helper scripts designed to be used by other packages for processing.

## How do I get set up? ##

To build and install this module from source please follow the below instructions.

### Required modules ###

The other required modules produced by Mark Driver as part of this work are:
    
- xmlvalidator [bitbucket](https://bitbucket.org/mdd31/xmlvalidator) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/utils/xmlvalidator)

### Clone the repository ###

*From bitbucket:*

    git clone https://bitbucket.org/mdd31/phasexmlparser.git
    

*From University of Cambridge gitlab (using ssh):*

    git clone git@gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/utils/phasexmlparser.git    

Change to the repository: 

    cd phasexmlparser

### Setting up the python environment ###

Details of an anaconda environment is provided in the repository with the required dependencies for this package.

	conda env create -f environment.yml

This creates an environment called 'resultsanalysis' which can be loaded using:

	conda activate resultsanalysis


### Using pip to install module ###

To install in your environment using pip run:

	pip install .

This installs it in your current python environment.
Test that it has been installed:

    ~$ ipython
    Python 3.7.5 (default, Oct 25 2019, 15:51:11) 
    Type 'copyright', 'credits' or 'license' for more information
    IPython 7.10.2 -- An enhanced Interactive Python. Type '?' for help.

    In [1]: import phasexmlparser.parsers.energyvaluesparsing

### Expected usage ###

This module contains methods for data extraction from phase and energy XML.
The submodules prvide methods for parsing information into structured dictionaries.
See documentation for individsual scripts for use cases and suggestions for
type of XML processed by module.

### Documentation ###

Documentation can be rendered using sphinx.

	cd docs
	make html

The rendered HTML can then be found in the build sub folder.

In progress: display of this documentation in a project wiki.

### Contribution guidelines ###

If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

### Who do I talk to? ###

Any queries please contact Mark Driver.

### License

&copy; Mark Driver,  Christopher Hunter, Teodor Nikolov at the University of Cambridge

This is released under an AGPLv3 license for academic use.
Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:

 

Cambridge Enterprise Ltd
University of Cambridge
Hauser Forum
3 Charles Babbage Rd
Cambridge CB3 0GT
United Kingdom
Tel: +44 (0)1223 760339
Email: software@enterprise.cam.ac.uk
